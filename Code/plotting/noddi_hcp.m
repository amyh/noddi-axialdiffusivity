%% NODDI with different assumed axial diffusivity

% The standard NODDI model was fitted to ten subjects from the HCP.
% We assume axial diffusivity = 1.7, 2.3 or 3

clear all
cluster = 1;
if cluster==1
    setenv( 'FSLDIR', '/opt/fmrib/fsl');
    fsldir = getenv('FSLDIR');
    fsldirmpath = sprintf('%s/etc/matlab',fsldir);
    path(path, fsldirmpath);
    clear fsldir fsldirmpath;
else  
    setenv( 'FSLDIR', '/usr/local/fsl');
    fsldir = getenv('FSLDIR');
    fsldirmpath = sprintf('%s/etc/matlab',fsldir);
    path(path, fsldirmpath);
    clear fsldir fsldirmpath;
end

ddir = '../../Data/HCP/ResultsInStandard/';
dax = [0.0017 0.0023 0.003];
for d = 1:numel(dax)
    %f = sprintf('%s/NODDI_Watson_diff_dax%d/',ddir,dax(d));
    f = [ddir  '/NODDI_Watson_dax' num2str(dax(d)) '_rician/'];
    fin(d,:,:,:) = double(read_avw([f '/mean_fintra_mean.nii.gz']));
    fiso(d,:,:,:) = double(read_avw([f '/mean_fiso_mean.nii.gz']));
    OD(d,:,:,:) = double(read_avw([f '/OD_mean.nii.gz']));
    disp(d)
end

mkdir([ddir '/PercentDifferenceMaps'])
save_avw(squeeze(fin(3,:,:,:)./fin(1,:,:,:))*100,...
    [ddir '/PercentDifferenceMaps/fintra.nii.gz'],'f',[1,1,1]);
save_avw(squeeze(fiso(3,:,:,:)./fiso(1,:,:,:))*100,...
    [ddir '/PercentDifferenceMaps/fiso.nii.gz'],'f',[1,1,1]);
save_avw(squeeze(OD(3,:,:,:)./OD(1,:,:,:))*100,...
    [ddir '/PercentDifferenceMaps/OD.nii.gz'],'f',[1,1,1]);

disp('Done')

fintot = fin.*(1-fiso);
fextot = (1-fin).*(1-fiso);
drad = (1-fin).*dax'.*1000;
for d = 1:numel(dax)
    %f = sprintf('%s/NODDI_Watson_diff_dax%0.1f/',ddir,dax(d));
    f = [ddir  '/NODDI_Watson_dax' num2str(dax(d)) '_rician/'];
    save_avw(squeeze(fintot(d,:,:,:)),...
        [f '/mean_fintratot_mean.nii.gz'],'f',[1,1,1]);
    save_avw(squeeze(fextot(d,:,:,:)),...
        [f '/mean_fextratot_mean.nii.gz'],'f',[1,1,1]);
    save_avw(squeeze(drad(d,:,:,:)),...
        [f '/drad_mean.nii.gz'],'f',[1,1,1]);
    disp(d)
end

save_avw(squeeze(fintot(3,:,:,:)./fintot(1,:,:,:))*100,...
    [ddir '/PercentDifferenceMaps/fintratot.nii.gz'],'f',[1,1,1]);
save_avw(squeeze(fextot(3,:,:,:)./fextot(1,:,:,:))*100,...
    [ddir '/PercentDifferenceMaps/fextratot.nii.gz'],'f',[1,1,1]);
save_avw(squeeze(drad(3,:,:,:)./drad(1,:,:,:))*100,...
    [ddir '/PercentDifferenceMaps/drad.nii.gz'],'f',[1,1,1]);

system(['for i in ' ddir '/PercentDifferenceMaps/*; do fslcpgeom $FSLDIR/data/standard/MNI152_T1_2mm_brain.nii.gz $i; fslmaths $FSLDIR/data/standard/MNI152_T1_2mm_brain_mask.nii.gz -mul $i $i; done'])
  
disp('Done')

%%
wm = double(read_avw('/home/fs0/amyh/func/standard/MNI152_T1_2mm_fast/MNI152_T1_2mm_brain_seg_2.nii.gz'));
gm = double(read_avw('/home/fs0/amyh/func/standard/MNI152_T1_2mm_fast/MNI152_T1_2mm_brain_seg_1.nii.gz'));

disp('---fiso---')
mean_mask(fiso,wm)
mean_mask(fiso,gm)
disp('---fintot---')
mean_mask(fintot,wm)
mean_mask(fintot,gm)
disp('---fextot---')
mean_mask(fextot,wm)
mean_mask(fextot,gm)
disp('---OD---')
mean_mask(OD,wm)
mean_mask(OD,gm)
disp('---drad---')
mean_mask(drad,wm)
mean_mask(drad,gm)

%% Plot histograms
%figure('units','normalized','outerposition',[0 0 1 1],'DefaultAxesFontSize',18)

[cb] = cbrewer('qual','Set3',10,'pchip');
col = [cb(5,:);cb(7,:);cb(6,:);cb(4,:);cb(8,:)];

brain = read_avw('$FSLDIR/data/standard/MNI152_T1_2mm_brain_mask.nii.gz');
brain(brain==0) = nan;
% Separate wm and gm
wm = read_avw('~/func/standard/MNI152_T1_2mm_fast/MNI152_T1_2mm_brain_seg_2.nii.gz');
wm(wm==0) = nan;
gm = read_avw('~/func/standard/MNI152_T1_2mm_fast/MNI152_T1_2mm_brain_seg_1.nii.gz');
gm(gm==0) = nan;

figure
subplot(3,5,1)
p = 'mean_fiso';
plot_hist(p,col,brain,'-')
legend('1.7','2.3','3.0')
ylabel('# voxels')
subplot(3,5,6)
plot_hist(p,col,wm,'--')
xlim([0,1])
ylabel('# voxels')
subplot(3,5,11)
plot_hist(p,col,gm,':')
xlabel('fiso')
ylabel('# voxels')

subplot(3,5,2)
p = 'mean_fintratot';
plot_hist(p,col,brain,'-')
subplot(3,5,7)
plot_hist(p,col,wm,'--')
subplot(3,5,12)
plot_hist(p,col,gm,':')
xlabel('fin * faniso')

subplot(3,5,3)
p = 'mean_fextratot';
plot_hist(p,col,brain,'-')
subplot(3,5,8)
plot_hist(p,col,wm,'--')
subplot(3,5,13)
plot_hist(p,col,gm,':')
xlabel('fex * faniso')

subplot(3,5,4)
p = 'OD';
plot_hist(p,col,brain,'-')
subplot(3,5,9)
plot_hist(p,col,wm,'--')
subplot(3,5,14)
plot_hist(p,col,gm,':')
xlabel('ODI')

subplot(3,5,5)
p = 'drad';
plot_hist(p,col,brain,'-')
xlim([0,3])
subplot(3,5,10)
plot_hist(p,col,wm,'--')
xlim([0,3])
subplot(3,5,15)
plot_hist(p,col,gm,':')
xlim([0,3])
xlabel('drad')

set(findall(gcf,'-property','FontSize'),'FontSize',12)

%%
save_fig('Figures/HCP-histograms',1)

%%
% Insets
figure
p = 'mean_fiso';
subplot(2,1,1)
plot_hist(p,col,wm,'--')
xlabel('fiso')
xlim([0,0.3])
ylim([0,1e4])

subplot(2,1,2)
plot_hist(p,col,gm,':')
xlabel('fiso')
xlim([0,0.3])
ylim([0,1e4])
set(findall(gcf,'-property','FontSize'),'FontSize',12)

%%
save_fig('Figures/HCP-histograms-inset',1)

%%
%%%%%%%


function plot_hist(param,col,mask,style)

ddir = '../../Data/HCP/ResultsInStandard/';

im1 = read_avw([ddir '/NODDI_Watson_dax0.0017_rician/' param '_mean.nii.gz']).*mask;
im2 = read_avw([ddir '/NODDI_Watson_dax0.0023_rician/' param '_mean.nii.gz']).*mask;
im3 = read_avw([ddir '/NODDI_Watson_dax0.003_rician/' param '_mean.nii.gz']).*mask;

%%
[h1,x1] = hist(im1(:),90);
[h2,x2] = hist(im2(:),90);
[h3,x3] = hist(im3(:),90);
plot(x1,h1,style,'Color',col(1,:),'LineWidth',2)
hold on
plot(x2,h2,style,'Color',col(2,:),'LineWidth',2)
plot(x3,h3,style,'Color',col(3,:),'LineWidth',2)
axis square
end


function mean_mask(im,mask)

x = squeeze(im(3,:,:,:)./im(1,:,:,:))*100;
x(mask==0) = [];
x(isnan(x)) = [];
x(x==0) = [];
disp(mean(x(:)))

end

    