%% Plotting axial diffusivity and ODI from in vivo data

% Here we have optimised the modified axial diffusivity model on both
% magnitude and real-valued in vivo data. The model is optimised to the
% signal of N concatenated voxels from the corpus callosum. N = 1, 25, 50,
% and 100 

clear all
addpath(genpath('../'))
[cb] = cbrewer('qual','Set3',10,'pchip');
col = [cb(5,:);cb(7,:);cb(6,:);cb(4,:);cb(8,:)];

% Folder with subject data
folder = '../../Data/MGH-RealValued/';
sub = dir([folder '/MGH_*']);

% Options are Gaussian w-offset, Rician w-offset and Rician wo-offset
% Rician w-offset uses priors to contrain upper and lower bound of the
% offset and noisefloor parameters. This is to limit the effect of
% degeneracy when estimating both noise parameters.

% The Gaussian model is fitted to real-valued data. The Rician model is
% fitted to magnitude data.

noise = 'Gaussian';
noise = 'Rician';
model = 'w-offset';
%model = 'wo-offset';   % Only tested for Rician

% Loop over N and read in the mean and std of the estimated dax and ODI
% samples from MCMC

Nall = [1 25 50 100];

for n = 1:numel(Nall)
    
    for ss = 1:numel(sub)

        if strcmp(noise,'Gaussian')
            f = [folder sub(ss).name '/diff_real/Delta49/HighB/'];
            fout = [f '/ModifiedNODDI_symm_priors0_smt1_1-' num2str(Nall(n)) '_offset_noisefloor0_mcmc.mat'];
        elseif strcmp(noise,'Rician') && strcmp(model,'w-offset')
            f = [folder sub(ss).name '/diff/Delta49/HighB/'];
            fout = [f '/ModifiedNODDI_symm_priors1_smt1_1-' num2str(Nall(n)) '_offset_noisefloor_mcmc.mat'];
        elseif strcmp(noise,'Rician') && strcmp(model,'wo-offset')
            f = [folder sub(ss).name '/diff/Delta49/HighB/'];
            fout = [f '/ModifiedNODDI_symm_priors0_smt1_1-' num2str(Nall(n)) '_offset0_noisefloor_mcmc.mat'];    
        end
    
        try
            load(fout)
            s = mean(samples,1);
            sstd = std(samples,[],1);
        catch
            warning(['Missing file: ' fout])
            s = nan.*s;
            sstd = nan.*sstd;
        end
                
        ODI(ss,n) = s(1);
        dax(ss,n) = s(2);
        offset(ss,n) = s(6);
        noisefloor(ss,n) = s(7);
        
        ODIstd(ss,n) = sstd(1);
        daxstd(ss,n) = sstd(2);
        
        if Nall(n)==100
            disp(const.S0/const.noiseg)
        end
        
    end
    
end
   
disp('Data loaded')
%%%

% Plot as a function of approximate SNR (16.5 ~ lower bound) rather than
% number of voxels
SNR = 16.5*sqrt(Nall);
figure
subplot(2,4,1)
plot(SNR,ODI','.-','LineWidth',1,'MarkerSize',12)
xlim([0,max(SNR)+25])
ylim([0.01,0.05])
ylabel('ODI')
xlabel('SNR')
axis square

subplot(2,4,5)
plot(SNR,ODIstd','.-','LineWidth',1,'MarkerSize',10)
xlim([0,max(SNR)+25])
ylim([0,24e-3])
ylabel('ODI std')
xlabel('SNR')
axis square

subplot(2,4,2)
plot(SNR,dax','.-','LineWidth',1,'MarkerSize',10)
xlim([0,max(SNR)+25])
ylim([1.5,3.5])
ylabel('axial diffusivity')
xlabel('SNR')
axis square

subplot(2,4,6)
plot(SNR,daxstd','.-','LineWidth',1,'MarkerSize',10)
xlim([0,max(SNR)+25])
ylim([0,0.7])
ylabel('axial diffusivity std')
xlabel('SNR')
axis square

subplot(2,4,3)
plot(SNR,offset','.-','LineWidth',1,'MarkerSize',10)
xlim([0,max(SNR)+25])
ylim([5,20])
ylabel('offset')
xlabel('SNR')
axis square

subplot(2,4,4)
plot(SNR,noisefloor','.-','LineWidth',1,'MarkerSize',10)
xlim([0,max(SNR)+25])
% ylim([0,0.5])
ylabel('noisefloor')
xlabel('SNR')
axis square
set(findall(gcf,'-property','FontSize'),'FontSize',14)

disp('----dax------')
disp(mean(dax,1))
disp('----ODI------')
disp(mean(ODI,1))

%%
save_fig(['Figures/invivo-data-' noise '-' model],1)


