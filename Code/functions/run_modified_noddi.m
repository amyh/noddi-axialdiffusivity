
function run_modified_noddi(files,const,sim,grid,plotflag)

% Run modified noddi model for either in vivo or simulated data
% files         paths to data
% const         constant model parameters
% sim           parameters for simulations (if simulating data)
% grid          parameters for gridsearch
% plotflag      =1 => plots basic output

% Please see run_simulations.m or run_modelling.m for how to set variables

rng('shuffle')          % shuffle random number generator

if ~exist('plotflag','var')
    plotflag = 0;
end

if ~exist([files.out(1:end-4) '_mcmc.mat'],'file')
%%
%%%% LOAD DATA %%%%
const = load_data(files,const);
sprintf('Data loaded')

%%
%%%% SIMULATE DATA %%%
if sim.flag==1
    const = simulate_data(const,sim);
end

%%
%%%% GRIDSEARCH %%%%
% Fix offset and noisefloor if required
if ~isempty(const.fixedoffset)
    grid.offset_s = [const.fixedoffset;const.fixedoffset];
end

if ~isempty(const.fixednoisefloor)
    grid.noisefloor_s = [const.fixednoisefloor;const.fixednoisefloor];
end

% Run gridsearch
grid = gridsearch(files,const,grid);


% Plot output from gridsearch
if plotflag
    plot_gridsearch(const,grid);
end

% Extract values from maxiumum posterior
grid = max_posterior(files,const,grid);

disp('Gridsearch completed')

%%
%%%% MCMC %%%%
samples = mcmc(files,const,grid);

% Plot mcmc output
if plotflag
    plot_mcmc(const,samples)
end

% Optimised parameters (saved in .mat file)
disp(mean(samples,1))
disp('MCMC completed')

else
    disp('File already exists. Did not overwrite')
end

end

