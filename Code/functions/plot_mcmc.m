%%%%%%%%%%%%%%%%%%%%%%%%%%%%% plot_mcmc %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

function plot_mcmc(const,samples)

% Basic plotting

addpath(genpath('../'))
[cb] = cbrewer('qual','Set3',10,'pchip');
col = [cb(5,:);cb(7,:);cb(6,:);cb(4,:);cb(8,:)];

const.output = 'predict';
p = pred(mean(samples,1),const);

figure
subplot(1,3,1)
plotmatrix(samples(:,[1,2,5,6,7]));
subplot(1,3,2), hold on
B = unique(const.bvals);
ang = (const.bvecs'*[0;0;1]).^2;
for bb = 1:3
    ind = const.bvals==B(bb);
    [as,ai] = sort(ang(ind));
    pb = p(ind);
    plot(as,log10(pb(ai)),'-','Color',col(bb,:),'Linewidth',2)
    s = const.S(ind);
    plot(as,log10(s(ai)),'x','Color',col(bb,:),'Linewidth',2)
end
xlabel('cos^2')
ylabel('log10 S')

subplot(1,3,3), hold on
for bb = 1:3
    ind = const.bvals==B(bb);
    [as,ai] = sort(ang(ind));
    pb = p(ind);
    plot(as,pb(ai),'-','Color',col(bb,:),'Linewidth',2)
    s = const.S(ind);
    plot(as,s(ai),'x','Color',col(bb,:),'Linewidth',2)
end
xlabel('cos^2')
ylabel('S')

end
