%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% gridsearch %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

function grid = gridsearch(files,const,grid)

    % Perform gridsearch, calculating expectation for each set of dax, ODI,
    % offset and noisefloor

    const.output = 'predict';
    
    % If priors flag=1, restrict ub and lb to of offset and noisefloor to be
    % 50 - 150% of const.noisemu and const.noiseg, which in real data, are
    % estimated from the ventricles
    if isfield(const,'priors') && const.priors==1
        tol = 0.5;
        grid.offset_s = linspace(const.noisemu*(1-tol),const.noisemu*(1+tol),numel(grid.offset_s));
        grid.noisefloor_s = linspace(const.noiseg*(1-tol),const.noiseg*(1+tol),numel(grid.noisefloor_s));
    end
        
    % Use SMT model for gridsearch (smaller param space to search)
    tmp = const.smt;
    const.smt = 1;

    % Calculate log likelihood for all combinations of params
    LL3D = nan(numel(grid.dax_s),numel(grid.ODI_s),numel(grid.offset_s),numel(grid.noisefloor_s));
    for i=1:numel(grid.dax_s)
        for j=1:numel(grid.ODI_s)
            for k=1:numel(grid.offset_s)
                for l = 1:numel(grid.noisefloor_s)
                    Sn = pred([grid.ODI_s(j),grid.dax_s(i),const.th,const.phi,0,grid.offset_s(k),grid.noisefloor_s(l)],const);
                    LL3D(i,j,k,l) = sum(-(const.S-Sn).^2./const.noiseg^2./2,'omitnan');      % Assumes Gaussian noise
                end
            end
        end
        sprintf('%d \n',i)
    end

    grid.LL3D = LL3D;   
    LL3D = LL3D-max(LL3D(:));

    % Expectation
    % (uninformative priors)
    e3D = exp(LL3D);
    e3D = e3D./sum(e3D(:),'omitnan');
    grid.e3D = e3D;
    
    const.smt = tmp;
    
    % Save output
    save(files.out,'grid','const','files')
    
end