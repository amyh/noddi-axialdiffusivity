# Run NODDI model with various axial diffusivity on HCP subjects
# Use MCMC optimisation with rician noise modelling
# Cudimot code

p=../../Data/HCP/
for i in 100207 100307 100408 100610 101006 101107 101309 101410 101915 102008; do 
    for d in 17 23 3; do 
        ./run-NODDI.sh $p/$i/diff/preproc/data -m invivo --dax 0.00${d} --diso 0.003 --out data.NODDI_Watson_dax0.00${d}_rician --runMCMC --rician; 
    done 
done


