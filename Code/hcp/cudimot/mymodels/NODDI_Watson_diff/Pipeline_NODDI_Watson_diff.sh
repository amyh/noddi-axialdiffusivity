#!/bin/sh
#
#   Moises Hernandez-Fernandez - FMRIB Image Analysis Group
#
#   Copyright (C) 2004 University of Oxford
#
#   SHCOPYRIGHT
#
# Pipeline for fitting NODDI-Watson
# Now with user specified dax and diso - Amy Howard Apr 2020

if [ "x$CUDIMOT" == "x" ]; then
	echo ""
	echo "Please, set enviroment variable CUDIMOT with the path where cuDIMOT is installed"
	echo "The path must contain a bin directory with binary files, i.e. \$CUDIMOT/bin"
	echo "For instance:   export CUDIMOT=/home/moises/CUDIMOT"
	echo ""
  exit 1
fi

bindir=${CUDIMOT}/bin

make_absolute(){
    dir=$1;
    if [ -d ${dir} ]; then
	OLDWD=`pwd`
	cd ${dir}
	dir_all=`pwd`
	cd $OLDWD
    else
	dir_all=${dir}
    fi
    echo ${dir_all}
}
Usage() {
    echo ""
    echo "Usage: Pipeline_NODDI_Watson_diff.sh <subject_directory> [options]"
    echo ""
    echo "expects to find data and nodif_brain_mask in subject directory"
    echo ""
    echo "<options>:"
    echo "--dax (axial diffusivity in m^2/s e.g. 0.0017)"
    echo "--diso (isotropic diffusivity in m^2/s e.g. 0.003)"
		echo "--out (output directory, subjdir.NODDI_Watson_diff by default)"
    echo "-Q (name of the GPU(s) queue, default cuda.q (defined in environment variable: FSLGECUDAQ)"
    echo "-NJOBS (number of jobs to queue, the data is divided in NJOBS parts, usefull for a GPU cluster, default 4)"
    echo "--runMCMC (if you want to run MCMC)"
    echo "--rician (if you want to run rician noise modelling)"
    echo "-b (burnin period, default 5000)"
    echo "-j (number of jumps, default 1250)"
    echo "-s (sample every, default 25)"
    echo "--BIC_AIC (calculate BIC & AIC)"
    echo ""
    exit 1
}

[ "$1" = "" ] && Usage

modelname=NODDI_Watson_diff
step1=GridSearch
step2=FitFractions

# Have removed this line 12/12/2022 as it was producing errors when copying across bvecs and bvals. Not sure why.
#export LD_LIBRARY_PATH=${LD_LIBRARY_PATH}:${FSLDIR}/lib

subjdir=`make_absolute $1`
subjdir=`echo $subjdir | sed 's/\/$/$/g'`

echo "---------------------------------------------------------------------------------"
echo "------------------------------------ CUDIMOT ------------------------------------"
echo "----------------------------- MODEL: $modelname -----------------------------"
echo "---------------------------------------------------------------------------------"
echo subjectdir is $subjdir

start=`date +%s`

#parse option arguments
njobs=1
burnin=1000
njumps=1250
sampleevery=25
other=""
queue=""
lastStepModelOpts=""
dax=0.0017
diso=0.003
outmain=${subjdir}.${modelname}

shift
while [ ! -z "$1" ]
do
  case "$1" in
      --dax) dax=$2;shift;;
      --diso) diso=$2;shift;;
			--out) outmain=$2;shift;;
      -Q) queue="-q $2";shift;;
      -NJOBS) njobs=$2;shift;;
      -b) burnin=$2;shift;;
      -j) njumps=$2;shift;;
      -s) sampleevery=$2;shift;;
      --runMCMC) lastStepModelOpts=$lastStepModelOpts" --runMCMC";;
      --BIC_AIC) lastStepModelOpts=$lastStepModelOpts" --BIC_AIC";;
      --rician) lastStepModelOpts=$lastStepModelOpts" --rician";;
      #*) other=$other" "$1;;
  esac
  shift
done

#Set options
opts="--bi=$burnin --nj=$njumps --se=$sampleevery"
opts="$opts $other"

if [ "x$SGE_ROOT" != "x" ]; then
	queue="-q $FSLGECUDAQ"
fi

#check that all required files exist

if [ ! -d $subjdir ]; then
	echo "subject directory $1 not found"
	exit 1
fi

if [ `${FSLDIR}/bin/imtest ${subjdir}/data` -eq 0 ]; then
	echo "${subjdir}/data not found"
	exit 1
fi

if [ `${FSLDIR}/bin/imtest ${subjdir}/nodif_brain_mask` -eq 0 ]; then
	echo "${subjdir}/nodif_brain_mask not found"
	exit 1
fi

if [ -e $outmain/xfms/eye.mat ]; then
	echo "${subjdir} has already been processed: $outmain."
	echo "Delete or rename $outmain before repeating the process."
	exit 1
fi

outmain=`make_absolute $outmain`
echo Output directory is $outmain
echo Making output directory structure

mkdir -p $outmain/
mkdir -p $outmain/diff_parts
mkdir -p $outmain/logs
mkdir -p $outmain/Dtifit
mkdir -p $outmain/${step1}
mkdir -p $outmain/${step1}/diff_parts
mkdir -p $outmain/${step1}/logs
mkdir -p $outmain/${step2}
mkdir -p $outmain/${step2}/diff_parts
mkdir -p $outmain/${step2}/logs
part=0

echo Copying files to output directory

${FSLDIR}/bin/imcp ${subjdir}/nodif_brain_mask $outmain
if [ `${FSLDIR}/bin/imtest ${subjdir}/nodif` = 1 ] ; then
    ${FSLDIR}/bin/fslmaths ${subjdir}/nodif -mas ${subjdir}/nodif_brain_mask $outmain/nodif_brain
fi

# Specify Common Fixed Parameters
CFP_file=$outmain/CFP
cp ${subjdir}/bvecs $outmain
cp ${subjdir}/bvals $outmain
echo  $outmain/bvecs > $CFP_file
echo  $outmain/bvals >> $CFP_file
N=`wc -w ${subjdir}/bvals | awk '{print $1;}'`
for dparam in dax diso; do
    for file in $outmain/$dparam $outmain/ddtmp; do
        if [ -f $file ]; then rm $file; fi
    done
    if [ "$dparam" == "dax" ]; then d=$dax; fi
    if [ "$dparam" == "diso" ]; then d=$diso; fi
    for ((i=1;i<=$N;i++)); do echo $d >> $outmain/ddtmp; done
    echo `cat $outmain/ddtmp` >  $outmain/$dparam
    echo  $outmain/$dparam >> $CFP_file
done
rm $outmain/ddtmp

#Set more options
opts=$opts" --data=${subjdir}/data --maskfile=$outmain/nodif_brain_mask --forcedir --CFP=$CFP_file"

# Calculate S0 with the mean of the volumes with bval<50
bvals=`cat ${subjdir}/bvals`
mkdir -p $outmain/temporal
pos=0
for i in $bvals; do
    if (( $(echo "$i < 50" |bc -l) )); then
       	fslroi ${subjdir}/data  $outmain/temporal/volume_$pos $pos 1
    fi
    pos=$(($pos + 1))
done
fslmerge -t $outmain/temporal/S0s $outmain/temporal/volume*
fslmaths $outmain/temporal/S0s -Tmean $outmain/S0
rm -rf $outmain/temporal

# Specify Fixed parameters: S0
FixPFile=$outmain/FixP
echo $outmain/S0 >> $FixPFile

##############################################################################
################################ First Dtifit  ###############################
##############################################################################
echo "Queue Dtifit"
PathDTI=$outmain/Dtifit
dtifit_command="${bindir}/Run_dtifit.sh ${subjdir} $outmain ${bindir}"
#SGE
dtifitProcess=`${FSLDIR}/bin/fsl_sub $queue -l $PathDTI/logs -N dtifit $dtifit_command`

##### Model Parameters: fiso, fintra, kappa, th, ph  ######
#############################################################
##################### Grid Search Step ######################
#############################################################
echo "Queue GridSearch process"
PathStep1=$outmain/${step1}

# Create file to specify initialisation parameters (2 parameters: th,ph)
InitializationFile=$PathStep1/InitializationParameters
echo "" > $InitializationFile #fiso
echo "" >> $InitializationFile #fintra
echo "" >> $InitializationFile #kappa
echo ${PathDTI}/dtifit_V1_th.nii.gz >> $InitializationFile #th
echo ${PathDTI}/dtifit_V1_ph.nii.gz >> $InitializationFile #ph

# Do GridSearch (fiso,fintra,kappa)
GridFile=$PathStep1/GridSearch
echo "search[0]=(0.0,0.1,0.2,0.3,0.4,0.5,0.6,0.7,0.8,0.9,1.0)" > $GridFile #fiso
echo "search[1]=(0.0,0.1,0.2,0.3,0.4,0.5,0.6,0.7,0.8,0.9,1.0)" >> $GridFile #fintra
echo "search[2]=(1,2,3,4,5,6,7,8)" >> $GridFile #kappa

partsdir=$PathStep1/diff_parts
outputdir=$PathStep1
Step1Opts=$opts" --outputdir=$outputdir --partsdir=$partsdir --FixP=$FixPFile --gridSearch=$GridFile --no_LevMar --init_params=$InitializationFile --fixed=3,4"

postproc=`${bindir}/jobs_wrapper.sh $PathStep1 $dtifitProcess $modelname GS $njobs $Step1Opts`

###############################################################
##################### Fit only Fractions ######################
###############################################################
echo "Queue Fitting Fractions process"
PathStep2=$outmain/${step2}

# Create file to specify initialisation parameters (2 parameters: fiso,fintra)
InitializationFile=$PathStep2/InitializationParameters
echo $PathStep1/Param_0_samples > $InitializationFile #fiso
echo $PathStep1/Param_1_samples >> $InitializationFile #fintra
echo $PathStep1/Param_2_samples >> $InitializationFile #kappa
echo ${PathDTI}/dtifit_V1_th.nii.gz >> $InitializationFile #th
echo ${PathDTI}/dtifit_V1_ph.nii.gz >> $InitializationFile #ph

partsdir=$PathStep2/diff_parts
outputdir=$PathStep2
Step2Opts=$opts" --outputdir=$outputdir --partsdir=$partsdir --FixP=$FixPFile --init_params=$InitializationFile --fixed=2,3,4"

postproc=`${bindir}/jobs_wrapper.sh $PathStep2 $postproc $modelname FitFractions $njobs $Step2Opts`

######################################################################################
######################### Fit all the parameters of the Model ########################
######################################################################################
echo "Queue Fitting process"

# Create file to specify initialization parameters (5 parameters: fiso,fintra,kappa,th,ph)
InitializationFile=$outmain/InitializationParameters
echo $PathStep2/Param_0_samples > $InitializationFile #fiso
echo $PathStep2/Param_1_samples >> $InitializationFile #fintra
echo ${PathStep1}/Param_2_samples >> $InitializationFile #kappa
echo ${PathDTI}/dtifit_V1_th.nii.gz >> $InitializationFile #th
echo ${PathDTI}/dtifit_V1_ph.nii.gz  >> $InitializationFile #ph

partsdir=$outmain/diff_parts
outputdir=$outmain
ModelOpts=$opts" --outputdir=$outputdir --partsdir=$partsdir --FixP=$FixPFile --init_params=$InitializationFile $lastStepModelOpts"

postproc=`${bindir}/jobs_wrapper.sh $outmain $postproc $modelname FitProcess $njobs $ModelOpts`

#########################################
### Calculate Dispersion Index & dyads ###
##########################################
finish_command="/home/fs0/amyh/cudimot/mymodels/NODDI_Watson_diff/${modelname}_finish.sh $outmain"
#SGE
finishProcess=`${FSLDIR}/bin/fsl_sub $queue -l $outmain/logs -N ${modelname}_finish -j $postproc $finish_command`

endt=`date +%s`
runtime=$((endt-start))
#echo Runtime $runtime
echo Everything Queued
