#!/bin/bash

# Average NODDI maps across subjs

d=$1/ResultsInStandard/
for modelname in NODDI_Watson_dax0.0017_rician NODDI_Watson_dax0.0023_rician NODDI_Watson_dax0.003_rician; do

    folder=$p/data.${modelname}/
    for param in mean_fintra mean_fiso OD; do

        # calculate average map
        fslmerge -t $d/${modelname}/${param}_combined.nii.gz $d/${modelname}/${param}/*gz
        fslmaths $d/${modelname}/${param}_combined.nii.gz -Tmean $d/${modelname}/${param}_mean.nii.gz

    done
done
