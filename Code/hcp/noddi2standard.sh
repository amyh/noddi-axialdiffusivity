#!/bin/bash

# Warp noddi output to standard space 

sub=`realpath $1`
p=$sub/diff/preproc/

ref=$FSLDIR/data/standard/MNI152_T1_2mm_brain.nii.gz
warp=$p/data.bedpostX/xfms/diff2standard_warp.nii.gz

d=$sub/../ResultsInStandard/
if [ ! d $d ]; then mkdir $d; fi
for modelname in NODDI_Watson_dax0.0017_rician NODDI_Watson_dax0.0023_rician NODDI_Watson_dax0.003_rician; do

    folder=$p/data.${modelname}/

    for param in mean_fintra mean_fiso OD; do

        out=$d/${modelname}/${param}/${1}_${param}.nii.gz
        if [ -f $out ]; then rm $out; fi

        mkdir -p $d/${modelname}/${param}

        in=$folder/${param}.nii.gz
        applywarp -i $in -o $out -r $ref -w $warp

    done
done

