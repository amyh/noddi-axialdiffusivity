%% parameter degeneracies - full model

% Ground truth simulation paramters
% dax = 2.2, ODI = 0.03, fin = 0.6, offset = 10

% Data was simulated to be both Gaussian and Rician distributed where the
% SNR of a single voxel = 16.5. The model was fitted to the average signal
% across 100 voxels.

clear all
addpath(genpath('../'))
[cb] = cbrewer('qual','Set3',10,'pchip');
col = [cb(5,:);cb(7,:);cb(6,:);cb(4,:);cb(8,:)];
rep = 1;

% Folder with simulated data
f = '../../Data/MGH-RealValued/MGH_002/diff_real/Delta49/HighB/simulations/';

% Noise
noise = 'Gaussian';
noise = 'Rician';
model = 'w-offset';
model = 'wo-offset';

if strcmp(noise,'Gaussian')
    base = 'ModifiedNODDI_priors0_smt1_1-100_offset_noisefloor0';     % Full model
    foutsim = [f '/' base '_simNvox100_simSNR16.5_' noise '_rep' num2str(rep) '_GT0.03_2.2_0.6_10_mcmc.mat'];
elseif strcmp(noise,'Rician') && strcmp(model,'w-offset')
    base = 'ModifiedNODDI_priors1_smt1_1-100_offset_noisefloor';     % Full model
    foutsim = [f '/' base '_simNvox100_simSNR16.5_' noise '_rep' num2str(rep) '_GT0.03_2.2_0.6_10_mcmc.mat'];
elseif strcmp(noise,'Rician') && strcmp(model,'wo-offset')
    base = 'ModifiedNODDI_priors0_smt1_1-100_offset0_noisefloor';     % Full model
    foutsim = [f '/' base '_simNvox100_simSNR16.5_' noise '_rep' num2str(rep) '_GT0.03_2.2_0.6_0_mcmc.mat'];
end

load(foutsim)
disp('Data loaded')

const.output = 'predict';
p = pred(mean(samples,1),const);


%%

% Axis ranges and GT values
xx = [0.02,0.04];
yy = [1.5,3.1];
zz = [7,13];
tt = [10,14];

xx = [0.01,0.05];
yy = [1.5,3.5];
zz = [0,20];
tt = [0,20];

xgt = 0.03;
ygt = 2.2;
zgt = 10;
tgt = const.noiseg;

% Plotting colour
c = cb(1,:);  
c = [.8 .8 1];

%figure('units','normalized','outerposition',[0 0 1 1])
figure
subplot(4,4,1)
H = histogram(samples(:,1));
H.FaceColor = c; 
xlim(xx)
%ylim([0,150])
axis square
hold on
plot([xgt xgt],[0,max(H.Values)+10],'--','Color',[0.5 0.5 0.5],'LineWidth',2)
box on

subplot(4,4,2), hold on
plot([ygt ygt],xx,'--','Color',[0.5 0.5 0.5],'LineWidth',2)
plot(yy,[xgt xgt],'--','Color',[0.5 0.5 0.5],'LineWidth',2)
try
    dscatter(samples(:,2),samples(:,1))
catch
    plot(samples(:,2),samples(:,1),'+','Color',c)
end
ylim(xx)
xlim(yy)
axis square
box on


subplot(4,4,3), hold on
plot([zgt zgt],xx,'--','Color',[0.5 0.5 0.5],'LineWidth',2)
plot(zz,[xgt xgt],'--','Color',[0.5 0.5 0.5],'LineWidth',2)
try
    dscatter(samples(:,6),samples(:,1))
catch
    plot(samples(:,6),samples(:,1),'+','Color',c)
end
ylim(xx)
xlim(zz)
axis square
box on


subplot(4,4,4), hold on
plot(tt,[xgt xgt],'--','Color',[0.5 0.5 0.5],'LineWidth',2)
try
    dscatter(samples(:,7),samples(:,1))
catch
    plot(samples(:,7),samples(:,1),'+','Color',c)
end
ylim(xx)
xlim(tt)
axis square
box on


subplot(4,4,5), hold on
plot([xgt xgt],yy,'--','Color',[0.5 0.5 0.5],'LineWidth',2)
plot(xx,[ygt ygt],'--','Color',[0.5 0.5 0.5],'LineWidth',2)
try
    dscatter(samples(:,1),samples(:,2))
catch
    plot(samples(:,1),samples(:,2),'+','Color',c)
end
xlim(xx)
ylim(yy)
axis square
box on


subplot(4,4,6)
H = histogram(samples(:,2));
H.FaceColor = c; 
xlim(yy)
axis square
hold on
plot([ygt ygt],[0 max(H.Values)+10],'--','Color',[0.5 0.5 0.5],'LineWidth',2)
box on


subplot(4,4,7), hold on
plot([zgt zgt],yy,'--','Color',[0.5 0.5 0.5],'LineWidth',2)
plot(zz,[ygt ygt],'--','Color',[0.5 0.5 0.5],'LineWidth',2)
try
    dscatter(samples(:,6),samples(:,2))
catch
    plot(samples(:,6),samples(:,2),'+','Color',c)
end
xlim(zz)
ylim(yy)
axis square
box on


subplot(4,4,8), hold on
plot(tt,[ygt ygt],'--','Color',[0.5 0.5 0.5],'LineWidth',2)
try
    dscatter(samples(:,7),samples(:,2))
catch
    plot(samples(:,7),samples(:,2),'+','Color',c)
end
xlim(tt)
ylim(yy)
axis square
box on


subplot(4,4,9), hold on
plot([xgt xgt],zz,'--','Color',[0.5 0.5 0.5],'LineWidth',2)
plot(xx,[zgt zgt],'--','Color',[0.5 0.5 0.5],'LineWidth',2)
try
    dscatter(samples(:,1),samples(:,6))
catch
    plot(samples(:,1),samples(:,6),'+','Color',c)
end
xlim(xx)
ylim(zz)
axis square
box on


subplot(4,4,10), hold on
plot([ygt ygt],zz,'--','Color',[0.5 0.5 0.5],'LineWidth',2)
plot(yy,[zgt zgt],'--','Color',[0.5 0.5 0.5],'LineWidth',2)
try
    dscatter(samples(:,2),samples(:,6))
catch
    plot(samples(:,2),samples(:,6),'+','Color',c)
end
xlim(yy)
ylim(zz)
axis square
box on


subplot(4,4,11)
H = histogram(samples(:,6));
H.FaceColor = c; 
xlim(zz)
axis square
hold on
plot([zgt zgt],[0,max(H.Values)+10],'--','Color',[0.5 0.5 0.5],'LineWidth',2)
box on


subplot(4,4,12), hold on
plot(tt,[zgt zgt],'--','Color',[0.5 0.5 0.5],'LineWidth',2)
try 
    dscatter(samples(:,7),samples(:,6))
catch 
    plot(samples(:,7),samples(:,6),'+','Color',c)
end
xlim(tt)
ylim(zz)
axis square
box on


subplot(4,4,13), hold on
plot([xgt xgt],tt,'--','Color',[0.5 0.5 0.5],'LineWidth',2)
try
    dscatter(samples(:,1),samples(:,7))
catch
    plot(samples(:,1),samples(:,7),'+','Color',c)
end
xlim(xx)
ylim(tt)
axis square
box on


subplot(4,4,14), hold on
plot([ygt ygt],tt,'--','Color',[0.5 0.5 0.5],'LineWidth',2)
try
    dscatter(samples(:,2),samples(:,7))
catch
    plot(samples(:,2),samples(:,7),'+','Color',c)
end
xlim(yy)
ylim(tt)
axis square
box on


subplot(4,4,15), hold on
plot([zgt zgt],tt,'--','Color',[0.5 0.5 0.5],'LineWidth',2)
try
    dscatter(samples(:,6),samples(:,7))
catch
    plot(samples(:,6),samples(:,7),'+','Color',c)
end
xlim(zz)
ylim(tt)
axis square
box on


subplot(4,4,16)
H = histogram(samples(:,7));
H.FaceColor = c; 
xlim(tt)
%ylim([0,150])
axis square
box on

set(findall(gcf,'-property','FontSize'),'FontSize',14)

%%
save_fig(['Figures/simulations-model-fit-' noise '-' model],1)


%% Plot model fit to data and errors (for each b-value) 
% as a function of angle from the primary fibre orientation

figure
B = unique(const.bvals);
ang = (const.bvecs'*[0;0;1]).^2;
subplot(2,1,1)
hold on
for bb = 1:3
    ind = const.bvals==B(bb);
    [as,ai] = sort(ang(ind));
    pb = p(ind);
    plot(as,pb(ai),'-','Color',cb(bb+3,:),'Linewidth',1)
    s = const.S(ind);
    plot(as,s(ai),'.','Color',cb(bb+3,:),'Linewidth',2,'MarkerSize',8)
end
xlabel('cos^2')
ylabel('S') 
axis square
box on
ylim([-10 90])

e = const.S-p;
subplot(2,1,2)
hold on
for bb = 1:3
    ind = const.bvals==B(bb);
    [as,ai] = sort(ang(ind));
    eb = e(ind);
    plot(as,eb(ai),'.','Color',cb(bb+3,:),'MarkerSize',12)
end
xlabel('cos^2')
ylabel('Error')
axis square
box on
ylim([-4 4])

set(findall(gcf,'-property','FontSize'),'FontSize',14)

%%

save_fig(['Figures/simulations-model-fit-2-' noise '-' model],1)

