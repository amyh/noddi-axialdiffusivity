%%%%%%%%%%%%%%%%%%%%%%%%%%%% max_posterior %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

function grid = max_posterior(files,const,grid)

% Calc max posterior from gridsearch to use as starting parameter of MCMC

disp('Gridsearch:')
tmp = squeeze(sum(sum(sum(grid.e3D,4,'omitnan'),3,'omitnan'),2,'omitnan'));
[~,i1] = max(tmp);
grid.max.dax = grid.dax_s(i1);

tmp = squeeze(sum(sum(sum(grid.e3D,4,'omitnan'),3,'omitnan'),1,'omitnan'));
[~,i2] = max(tmp);
grid.max.ODI = grid.ODI_s(i2);

tmp = squeeze(sum(sum(sum(grid.e3D,4,'omitnan'),2,'omitnan'),1,'omitnan'));
[~,i3] = max(tmp);
grid.max.offset = grid.offset_s(i3);

tmp = squeeze(sum(sum(sum(grid.e3D,3,'omitnan'),2,'omitnan'),1,'omitnan'));
[~,i4] = max(tmp);
grid.max.noisefloor = grid.noisefloor_s(i4);

disp(['opt dax = ' num2str(grid.max.dax)])
disp(['opt ODI = ' num2str(grid.max.ODI)])
disp(['opt offset = ' num2str(grid.max.offset)])
disp(['opt noisefloor = ' num2str(grid.max.noisefloor)])

save(files.out,'grid','const','files')

end