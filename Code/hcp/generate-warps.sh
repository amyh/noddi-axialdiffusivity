#!/bin/bash

# Generate warpfields to register hcp diffusion 2 mni (via structural)

sub=`realpath $1`
r=$sub/diff/raw/
c=$r/CombinedData/
p=$sub/diff/preproc/
s=$sub/struct/T1/


# run bedpostx
FILE=$p/data.bedpostX/xfms
if [ -d "$FILE" ]; then rm -r $FILE; fi
bedpostx $p/data --f0 --ardf0

refbrain=$FSLDIR/data/standard/MNI152_T1_2mm_brain.nii.gz
ref=$FSLDIR/data/standard/MNI152_T1_2mm.nii.gz

if [ ! -d "$FILE" ]; then
    mkdir -p $FILE
fi

# Extract mean b0
~/func/extract_shell.py -d $p/data/data.nii.gz -b $p/data/bvals -v $p/data/bvecs -o $p/data/b0.nii.gz
fslmaths $p/data/b0 -Tmean $p/data/b0_mean

# create nodif_brain in bedpostx directory
fslmaths $p/data/b0_mean.nii.gz -mul $p/data/nodif_brain_mask.nii.gz $p/data.bedpostX/nodif_brain

# Prepare structural images
# bet extract
T1=T1w_acpc_dc_restore_1.25.nii.gz
ln -s $s/preproc/$T1 $s/preproc/t1.nii.gz
jid_bet=`fsl_sub -q short.q -l $s/preproc/ bet $s/preproc/t1 $s/preproc/t1_brain -m -R`

# register using FDT (bias restored and brain extracted)
# Linear registration diff to struct
jid1=`fsl_sub -q short.q -j $jid_bet -l $p/data.bedpostX/xfms $FSLDIR/bin/flirt -in $p/data.bedpostX/nodif_brain -ref $s/preproc/t1_brain.nii.gz -omat $p/data.bedpostX/xfms/diff2str.mat -searchrx -90 90 -searchry -90 90 -searchrz -90 90 -dof 6 -cost corratio`

# inverse transform
jid2=`fsl_sub -q short.q -l $p/data.bedpostX/xfms -j $jid1 $FSLDIR/bin/convert_xfm -omat $p/data.bedpostX/xfms/str2diff.mat -inverse $p/data.bedpostX/xfms/diff2str.mat`

# Linear registration struct to standard
jid3=`fsl_sub -q short.q -l $p/data.bedpostX/xfms -j $jid2 $FSLDIR/bin/flirt -in $s/preproc/t1_brain.nii.gz -ref $refbrain -omat $p/data.bedpostX/xfms/str2standard.mat -searchrx -90 90 -searchry -90 90 -searchrz -90 90 -dof 12 -cost corratio`

# inverse transform
jid4=`fsl_sub -q short.q -l $p/data.bedpostX/xfms -j $jid3 $FSLDIR/bin/convert_xfm -omat $p/data.bedpostX/xfms/standard2str.mat -inverse $p/data.bedpostX/xfms/str2standard.mat`

# concat transforms
jid5=`fsl_sub -q short.q -l $p/data.bedpostX/xfms -j $jid4 $FSLDIR/bin/convert_xfm -omat $p/data.bedpostX/xfms/diff2standard.mat -concat $p/data.bedpostX/xfms/str2standard.mat $p/data.bedpostX/xfms/diff2str.mat`

# inverse transform
jid6=`fsl_sub -q short.q -l $p/data.bedpostX/xfms -j $jid5 $FSLDIR/bin/convert_xfm -omat $p/data.bedpostX/xfms/standard2diff.mat -inverse $p/data.bedpostX/xfms/diff2standard.mat`

# Nonlinear registration struct to standard
jid7=`fsl_sub -q short.q -l $p/data.bedpostX/xfms -j $jid6 $FSLDIR/bin/fnirt --in=$s/preproc/t1.nii.gz --aff=$p/data.bedpostX/xfms/str2standard.mat --cout=$p/data.bedpostX/xfms/str2standard_warp --config=T1_2_MNI152_2mm`

# Invert and concat warps
jid8=`fsl_sub -q short.q -l $p/data.bedpostX/xfms -j $jid7 $FSLDIR/bin/invwarp -w $p/data.bedpostX/xfms/str2standard_warp -o $p/data.bedpostX/xfms/standard2str_warp -r $s/preproc/t1_brain.nii.gz`

jid9=`fsl_sub -q short.q -l $p/data.bedpostX/xfms -j $jid8 $FSLDIR/bin/convertwarp -o $p/data.bedpostX/xfms/diff2standard_warp -r $ref -m $p/data.bedpostX/xfms/diff2str.mat -w $p/data.bedpostX/xfms/str2standard_warp`

jid10=`fsl_sub -q short.q -l $p/data.bedpostX/xfms -j $jid9 $FSLDIR/bin/convertwarp -o $p/data.bedpostX/xfms/standard2diff_warp -r $p/data.bedpostX/nodif_brain_mask -w $p/data.bedpostX/xfms/standard2str_warp --postmat=$p/data.bedpostX/xfms/str2diff.mat`


# Apply warps so can asses quality of registration
jid11=`fsl_sub -q short.q -l $p/data.bedpostX/xfms -j $jid10 applywarp -i $p/data.bedpostX/nodif_brain -r $ref -o $p/data.bedpostX/xfms/diff2standard -w $p/data.bedpostX/xfms/diff2standard_warp`
jid12=`fsl_sub -q short.q -l $p/data.bedpostX/xfms -j $jid11 applywarp -i $s/preproc/t1.nii.gz -r $ref -o $p/data.bedpostX/xfms/str2standard -w $p/data.bedpostX/xfms/str2standard_warp`
