%%%%%%%%%%%%%%%%%%%%%%%%%%%%% hyp_Sapprox %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

function c1=hyp_Sapprox(x)
% Saddlepoint approximation of hypergeometric function of a matrix argument
% x:                - eigenvalues

% Author: Stam Sotiropulous

x=sort(x,'descend');
if (x(1)==0 && x(2)==0 && x(3)==0)
    c1=1;
else
    t=find_t(x);
    R=1; K2=0; K3=0; K4=0;
    for i=1:3
        R=R/sqrt(-x(i)-t);
        K2=K2+1/(2*(x(i)+t)^2);
        K3=K3-1/(x(i)+t)^3;
        K4=K4+3/(x(i)+t)^4;
    end;

    T=K4/(8*K2^2)-5*K3^2/(24*K2^3);  
    c1=sqrt(2/K2)*pi*R*exp(-t);
    c3=c1*exp(T);
    c1=c3;
end;
if(not(isreal(c1)))
      c1=real(c1); 
end


end % EOF

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

function z1=find_t(x)

l1=-x(1); l2=-x(2); l3=-x(3);

a3=l1*l2+l2*l3+l1*l3;
a2=1.5-l1-l2-l3;
a1=a3-l1-l2-l3;
a0=0.5*(a3-2*l1*l2*l3);

inv3=1/3;
p=(a1-a2*a2*inv3)*inv3;
q=(-9*a2*a1+27*a0+2*a2*a2*a2)/54;
D=q*q+p*p*p;
offset=a2*inv3;
if (D>0)
	ee=sqrt(D);
    tmp=-q+ee; z1=croot(tmp);
	tmp=-q-ee; z1=z1+croot(tmp);
	z1=z1-offset; z2=z1; z3=z1;
elseif (D<0)
	ee=sqrt(-D);
  	tmp2=-q; 
	angle=2*inv3*atan(ee/(sqrt(tmp2*tmp2+ee*ee)+tmp2));
	sqrt3=sqrt(3.0);
	tmp=cos(angle);
	tmp2=sin(angle);
	ee=sqrt(-p);
	z1=2*ee*tmp-offset; 
	z2=-ee*(tmp+sqrt3*tmp2)-offset; 
	z3=-ee*(tmp-sqrt3*tmp2)-offset; 
else
	tmp=-q;
	tmp=croot(tmp);
	z1=2*tmp-offset; z2=z1; z3=z1;
	if (p~=0 || q~=0)
	    z2=-tmp-offset; z3=z2;
	end;
end;

if(~isreal(z1)) z1=real(z1); end;
if(~isreal(z2)) z2=real(z2); end;
if(~isreal(z3)) z3=real(z3); end;

z1=min([z1 z2 z3]);

end % EOF

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

function res=croot(x)
inv3=1/3;
if (x>=0) 
    res=x^(inv3);
else
    res=-((-x)^(inv3));
end;

end % EOF