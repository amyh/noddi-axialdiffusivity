%% GETTING THE NOISE ASSUMPTIONS WRONG

% Here we simulate data and then fit it assuming ether that there is a
% fixed offset signal, or some fixed predefined noisefloor. This mimics the
% situation where noise parameters are first estimated from the data (for
% example, noise parameters may be estimated using various denoising
% methods) and then assumed to be known a priori during model fitting.

% Ground truth simulation paramters
% dax = 2.2, ODI = 0.03, fin = 0.6, offset = 10

% Data was simulated to be both Gaussian and Rician distributed where the
% SNR of a single voxel = 16.5. The model was fitted to the average signal
% across 100 voxels.

% Unless stated otherwise, here we have ran the simulations 20x (i.e. 20 
% repeats) and we plot the mean parameters across repetitions. 

%%
%%%% Assuming wrong offset %%%%

% Here we have simulated data with a signal offset = 10, and fitted
% assuming some fixed offset = [0:15]

% Plot the estimated axial diffusivity and ODI (mean&std) for each assumed
% offset to see how the parameters are biased when we get the offset
% wrong (e.g. if we estimated it externally rather than as a model
% parameter)

% Set priors=0 throughout as only one noise parameter is being estimated

clear all
addpath(genpath('../'))
[cb] = cbrewer('qual','Set3',10,'pchip');
col = [cb(5,:);cb(7,:);cb(6,:);cb(4,:);cb(8,:)];

% Folder with simulated data
f = '../../Data/MGH-RealValued/MGH_002/diff_real/Delta49/HighB/simulations/';

noise = 'Gaussian';
%noise = 'Rician';
GToffset = 10;
simN = 100;

fixedoffset = 0:15;  
for i = 1:numel(fixedoffset)  
    
    if strcmp(noise,'Gaussian')
        base = ['ModifiedNODDI_priors0_smt1_1-100_offset' num2str(fixedoffset(i)) '_noisefloor0'];    
    elseif strcmp(noise,'Rician')
        base = ['ModifiedNODDI_priors0_smt1_1-100_offset' num2str(fixedoffset(i)) '_noisefloor'];
    end
    
    for rep = 1:20
        
        foutsim = [f '/' base '_simNvox' num2str(simN) '_simSNR16.5_' noise '_rep' num2str(rep) '_GT0.03_2.2_0.6_10_mcmc.mat'];
        load(foutsim)

        ODI(i,:,rep) = samples(:,1);
        dax(i,:,rep) = samples(:,2);
    
    end
end

disp('Data loaded')

figure
subplot(2,2,1)
plot(fixedoffset,mean(mean(dax,2),3),'LineWidth',2,'Color',cb(1,:))
hold on
plot([GToffset,GToffset],[1,4],'--','Color',[0.5,0.5,0.5])
plot(fixedoffset,ones(size(fixedoffset))*2.2,'--','Color',[0.5,0.5,0.5])
ylabel('Axial diffusivity')
xlabel('Fixed Offset')
axis square

subplot(2,2,2)
plot(fixedoffset,mean(mean(ODI,2),3),'LineWidth',2,'Color',cb(1,:))
hold on
plot([GToffset,GToffset],[0,0.1],'--','Color',[0.5,0.5,0.5])
plot(fixedoffset,ones(size(fixedoffset))*0.03,'--','Color',[0.5,0.5,0.5])
ylabel('ODI')
xlabel('Fixed Offset')
axis square

subplot(2,2,3)
plot(fixedoffset,mean(std(dax,[],2),3),'LineWidth',2,'Color',cb(1,:))
hold on
plot([GToffset,GToffset],[0,0.6],'--','Color',[0.5,0.5,0.5])
ylabel('Axial diffusivity std')
xlabel('Fixed Offset')
axis square

subplot(2,2,4)
plot(fixedoffset,mean(std(ODI,[],2),3),'LineWidth',2,'Color',cb(1,:))
hold on
plot([GToffset,GToffset],[0,0.015],'--','Color',[0.5,0.5,0.5])
ylabel('ODI std')
xlabel('Fixed Offset')
axis square

set(findall(gcf,'-property','FontSize'),'FontSize',12)

save_fig(['Figures/simulations-wrong-offset-' noise],1)


%%
%%%% Assuming wrong noisefloor %%%%

% Here we have simulated data with a Rician noisefloor (equivalent to
% Gaussian data with SNR=16.5) and then fitted assuming some fixed 
% noisefloor = [0:25]

% Plot the estimated axial diffusivity and ODI (mean&std) for each assumed
% offset to see how the parameters are biased when we get the noisefloor
% wrong 

% To isolate the noisefloor effects from the signal offset, during model 
% fitting we fix the offset to the ground truth value (=0).

clear all
addpath(genpath('../'))
[cb] = cbrewer('qual','Set3',10,'pchip');
col = [cb(5,:);cb(7,:);cb(6,:);cb(4,:);cb(8,:)];

% Folder with simulated data
f = '../../Data/MGH-RealValued/MGH_002/diff_real/Delta49/HighB/simulations/';

noise = 'Rician';
simN = 100;

fixednoisefloor = 0:2:30;  
for i = 1:numel(fixednoisefloor)  
    
    base = ['ModifiedNODDI_priors0_smt1_1-100_offset0_noisefloor' num2str(fixednoisefloor(i))];
    
    for rep = 1:20
        
        foutsim = [f '/' base '_simNvox' num2str(simN) '_simSNR16.5_' noise '_rep' num2str(rep) '_GT0.03_2.2_0.6_0_mcmc.mat'];
        load(foutsim)

        ODI(i,:,rep) = samples(:,1);
        dax(i,:,rep) = samples(:,2);    
    
    end
end

GTnoisefloor = const.S0/16.5;    % What we would assume from in vivo data

disp('Data loaded')


figure
subplot(2,2,1)
plot(fixednoisefloor,mean(mean(dax,2),3),'LineWidth',2,'Color',cb(1,:))
hold on
plot([GTnoisefloor,GTnoisefloor],[1,4],'--','Color',[0.5,0.5,0.5])
plot(fixednoisefloor,ones(size(fixednoisefloor))*2.2,'--','Color',[0.5,0.5,0.5])
ylabel('Axial diffusivity')
xlabel('Fixed Noisefloor')
axis square

subplot(2,2,2)
plot(fixednoisefloor,mean(mean(ODI,2),3),'LineWidth',2,'Color',cb(1,:))
hold on
plot([GTnoisefloor,GTnoisefloor],[0,0.2],'--','Color',[0.5,0.5,0.5])
plot(fixednoisefloor,ones(size(fixednoisefloor))*0.03,'--','Color',[0.5,0.5,0.5])
ylim([0,0.1])
ylabel('ODI')
xlabel('Fixed Noisefloor')
axis square

subplot(2,2,3)
plot(fixednoisefloor,mean(std(dax,[],2),3),'LineWidth',2,'Color',cb(1,:))
hold on
plot([GTnoisefloor,GTnoisefloor],[0,0.7],'--','Color',[0.5,0.5,0.5])
ylim([0,0.6])
ylabel('Axial diffusivity std')
xlabel('Fixed Noisefloor')
axis square

subplot(2,2,4)
plot(fixednoisefloor,mean(std(ODI,[],2),3),'LineWidth',2,'Color',cb(1,:))
hold on
plot([GTnoisefloor,GTnoisefloor],[0,0.02],'--','Color',[0.5,0.5,0.5])
ylabel('ODI std')
xlabel('Fixed Noisefloor')
axis square
set(findall(gcf,'-property','FontSize'),'FontSize',12)

save_fig('Figures/simulations-wrong-noisefloor',1)

%%

% What happens if we assume Gaussian noise for magnitude data 
% i.e. we fix the noisefloor to 0

clear all
addpath(genpath('../'))
[cb] = cbrewer('qual','Set3',10,'pchip');
col = [cb(5,:);cb(7,:);cb(6,:);cb(4,:);cb(8,:)];

% Folder with simulated data
f = '../../Data/MGH-RealValued/MGH_002/diff_real/Delta49/HighB/simulations/';

noise = 'Rician';
simN = 100;

fixednoisefloor = 0; 
for i = 1:numel(fixednoisefloor)  
    
    base = ['ModifiedNODDI_priors0_smt1_1-100_offset0_noisefloor' num2str(fixednoisefloor(i))];
    
    for rep = 1
        
        foutsim = [f '/' base '_simNvox' num2str(simN) '_simSNR16.5_' noise '_rep' num2str(rep) '_GT0.03_2.2_0.6_0_mcmc.mat'];
        load(foutsim)
        
        figure
        try
            scatterhist(samples(:,1),samples(:,2),'Marker','+','Color',cb(1,:),'Kernel','on')
            %dscatter(samples(:,1),samples(:,2))
        catch
            plot(samples(:,1),samples(:,2),'+','Color',cb(1,:))
        end
        axis square
        hold on
        plot([0.03 0.03],[0,1.1*max(samples(:,2))],'--','Color',[0.5 0.5 0.5],'LineWidth',2)
        plot([0,1.1*max(samples(:,1))],[2.2 2.2],'--','Color',[0.5 0.5 0.5],'LineWidth',2)
        ylim([1,4])
        xlim([0,1.1*max(samples(:,1))])
        xlabel('ODI')
        ylabel('Axial diffusivity')
    end
end

set(findall(gcf,'-property','FontSize'),'FontSize',12)

save_fig('Figures/simulations-wrong-noise-assumption',1)

