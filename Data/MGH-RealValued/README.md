# Outputs from the modified NODDI model
Here we have the output from the modified NODDI model. For each subject, the 
model was applied to both real and magnitude data - see folders "diff" and 
"diff_real".
The output are matlab files, where the 'samples' variable is the estimated 
parameter distributions from metropolis hastings (~mcmc).

## Filenames
An example file name:
ModifiedNODDI_symm_priors0_smt1_1-50_offset0_noisefloor_mcmc.mat

- "symm" indicates that we assume symmetric dispersion about the mean
fibre orientation. When concatenating signal across many voxels, V2 
from DTI is randomly oriented, and the model assumes the FOD can be 
described by a Watson distribution. This is true for all results.

- "priors0" indicates that no priors were used when estimating either the 
noisefloor or signal offset. Priors are only used ("priors1") when both 
parameters are estimated, as they were otherwise found to be somewhat 
degenerate.

- "smt1" indicates that we utilise the spherical mean to avoid estimating 
F=f*S0. This is true for all results.

- "1-50" indicates that we optimise over the concatenated signal of 50 
voxels. Alternatives are "1-1", "1-25", "1-100"

- "offset0" indicates that the offset was fixed to 0 and not estimated as 
a parameter of the model. When "offset" is stated without the 0, the 
offset was estimated as a parameter of the model.

- "noisefloor" is similar to offset above. Here the noisefloor is estimated
as a parameter of the model.

- "mcmc" indicates that this is the output of mcmc. Grid search was run 
prior to mcmc to find starting values.


