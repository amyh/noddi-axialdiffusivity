
function run_simulations(noisemodel,Nvox,Nrep,fixedoffset,fixednoisefloor,offset)    

% Run simulations for modified NODDI model
% noisemodel        'Gaussian'/'Rician'
% Nvox              Number of voxels to average 
% Nrep              Number of repetitions 
% fixedoffset       If [] => estimated parameter of the model
% fixednoisefloor   =0 => Optimise assuming Gaussian. = [] => rician

% Set paths for fsl read_avw / write_avw
cluster = 1;
if cluster==1
    setenv( 'FSLDIR', '/opt/fmrib/fsl');
    fsldir = getenv('FSLDIR');
    fsldirmpath = sprintf('%s/etc/matlab',fsldir);
    path(path, fsldirmpath);
    clear fsldir fsldirmpath;
else  
    setenv( 'FSLDIR', '/usr/local/fsl');
    fsldir = getenv('FSLDIR');
    fsldirmpath = sprintf('%s/etc/matlab',fsldir);
    path(path, fsldirmpath);
    clear fsldir fsldirmpath;
end

% Select in vivo data for simulations to mirror
folder = '../Data/MGH-RealValued/';
sub = 'MGH_002/';       % Use sub02 as sub01 has large ventricles
diffreal = 'diff_real';
mask = 'AllCC_dil';

% Simulation parameters
sim.flag = 1;
sim.GT.ODI = 0.03;
sim.GT.dax = 2.2;
sim.GT.f = 0.6;
if ~exist('offset','var'), offset = 10; end
sim.GT.offset = offset;
sim.noisemodel = noisemodel;
sim.Nvox = Nvox;
sim.average = 1;    % average signal across voxels (instead of concatenating)
sim.SNR = 16.5;

% Parameters to load in vivo data which simulations are based on
files.main = [folder '/' '/' sub '/' diffreal '/Delta49/HighB/'];
files.mask = [folder '/' sub '/' diffreal '/masks/cc/' mask '.nii.gz'];
files.dti = [folder '/' sub '/' diffreal '/Delta49/LowB.dtifit/'];
files.vent = [folder '/' sub '/' diffreal '/Delta49/b17800/data_ventricles.nii.gz'];
const.rician = 0;       % estimates noise characteristics from data
const.N = 1:100;        % number of voxels to calculate S0 over
const.excess = 0.1;     % S0 similarity = +/- 10%

% Optimisation parameters
const.fixednoisefloor = fixednoisefloor;
const.fixedoffset = fixedoffset;
const.smt = 1;          % replace F=fin*S0 with spherical mean

% Priors can be used to constrain the parameter space (ub/lb) of the offset
% and noisefloor. Flag should be set to 0 if only one noise parameter is 
% estimated, and 1 if both are estimated
if isempty(const.fixednoisefloor) && isempty(const.fixedoffset)
    const.priors = 1;       % Restrict ub/.lb of offset and noisefloor according to values estimated from the ventricles
else
    const.priors = 0;
end

% Parameters of gridsearch (run prior to mh)
grid.dax_s = linspace(1.5,3,16);
grid.ODI_s = linspace(0.01,0.07,16);
grid.noisefloor_s = linspace(0,20,11);
grid.offset_s = linspace(0,20,11);

% Don't plot output
plotflag = 0;

% Run model for Nrep different instances of noise
for rep = 1:Nrep
    % Output file
    mkdir([files.main '/simulations/'])
    files.out = [files.main '/simulations/ModifiedNODDI_priors' num2str(const.priors) '_smt' num2str(const.smt) '_' num2str(min(const.N)) '-' num2str(max(const.N)) '_offset' num2str(const.fixedoffset) '_noisefloor' num2str(const.fixednoisefloor) '.mat'];
    if sim.flag
    files.out = [files.out(1:end-4) '_simNvox' num2str(sim.Nvox) '_simSNR' num2str(sim.SNR) '_' sim.noisemodel '_rep' num2str(rep) '_GT' num2str(sim.GT.ODI) '_' num2str(sim.GT.dax) '_' num2str(sim.GT.f) '_' num2str(sim.GT.offset) '.mat'];
    end

    % Run model
    run_modified_noddi(files,const,sim,grid,plotflag)
end

disp(files.out)

end

