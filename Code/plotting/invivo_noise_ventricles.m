%% Noise characteristics of invivo data

% Plot histogram of signal in ventricles at very high b-value, where there
% should be only noise. Repeat for both magnitude and real-valued data

% Note, throughout we use MGH_002 as an example subject as MGH_001 has
% abnormally large ventricles and so may be a bit atypical for the dataset

clear all
cd /vols/Scratch/amyh/AxialDiffusvityValidation/Code/plotting
addpath(genpath('../'))
[cb] = cbrewer('qual','Set3',10,'pchip');
col = [cb(5,:);cb(7,:);cb(6,:);cb(4,:);cb(8,:)];

cluster = 1;

if cluster==1
    setenv( 'FSLDIR', '/opt/fmrib/fsl');
    fsldir = getenv('FSLDIR');
    fsldirmpath = sprintf('%s/etc/matlab',fsldir);
    path(path, fsldirmpath);
    clear fsldir fsldirmpath;
else  
    setenv( 'FSLDIR', '/usr/local/fsl');
    fsldir = getenv('FSLDIR');
    fsldirmpath = sprintf('%s/etc/matlab',fsldir);
    path(path, fsldirmpath);
    clear fsldir fsldirmpath;
end

figure
for i = 1:2

    if i==1
        f = '../../DataMGH-RealValued/MGH_002/diff_real/Delta49/b17800/';
    else
        f = '../../DataMGH-RealValued/MGH_002/diff/Delta49/b17800/';
    end
    
    d = double(read_avw([f '/data_ventricles.nii.gz']));
    d(d==0) = [];

    subplot(2,1,i)
    if i==1
        h = histfit(d,50,'Normal');
        legend('Data','Fitted Gaussian')
        pd = fitdist(d','Normal');
        disp('---- Normal dist ----')
        disp(['Mean: ' num2str(pd.mu)])
    else
        h = histfit(d(d>0),50,'Rician');
        legend('Data','Fitted Rician')
        pd = fitdist(d(d>0)','Rician');
        disp('---- Rician dist ----')
        disp(['Scale: ' num2str(pd.s)])
    end
    
    disp(['Sigma: ' num2str(pd.sigma)])
    
    h(1).FaceColor = [.8 .8 1];
    h(2).Color = [.2 .2 .2];
    axis square
    xlabel('Signal')
    ylabel('Count')
    
end
%%

save_fig('Figures/MGH-ventricles',1)

