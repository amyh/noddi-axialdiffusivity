%%%%%%%%%%%%%%%%%%%%%%%%%%% plot_gridsearch %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

function grid = plot_gridsearch(const,grid)

% Basic plotting

figure
z = squeeze(sum(sum(grid.e3D,4,'omitnan'),3,'omitnan'))';
subplot(2,2,1)
contour(grid.dax_s,grid.ODI_s,z,200,'LineWidth',2);
axis square
xlabel('dax')
ylabel('odi')
hold on
title('sum squares')

subplot(2,2,2)
contour(grid.dax_s,grid.ODI_s,log(z),200,'LineWidth',2);
axis square
xlabel('dax')
ylabel('odi')
hold on
title('log likelihood')

subplot(2,2,3)
plot(grid.ODI_s,sum(z,2,'omitnan'))
xlabel('ODI')
ylabel('marg post')
axis square

subplot(2,2,4)
plot(grid.dax_s,sum(z,1,'omitnan'))
xlabel('dax')
ylabel('marg post')
axis square

% Check signal anisotropy
figure
B = unique(const.bvals);
for ss = 1:numel(B)
    subplot(1,numel(B),ss)
    S = const.S(const.bvals==B(ss));
    v = const.bvecs(:,const.bvals==B(ss));
    scatter3(S'.*v(1,:),S'.*v(2,:),S'.*v(3,:))
    hold on
    S=-S;
    scatter3(S'.*v(1,:),S'.*v(2,:),S'.*v(3,:))
    axis([-150 150 -150 150 -150 150])
    view(0,90)
    xlabel('x'),ylabel('y'),zlabel('z')
    axis square
end

end
