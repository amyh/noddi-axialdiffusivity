%%%%%%%%%%%%%%%%%%%%%%%%%%%% modified_NODDI %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

function Y = modified_NODDI(P,g,b,const)

% modified NODDI model for high b data
% S = F * 1F1(kappa*(mu'mu)-b*d*(g'g)) / 1F1(kappa*(mu'mu)

%%%% If Rician dist noise %%%%
% Y = sqrt((S+offset)^2+noisefloor^2)
    
%%%% If Gaussian dist noise %%%%
% Y = S+offset

%%%% If const.smt=1 replace F ith spherical mean %%%%
% F = Sbar*sqrt(b*d)/(sqrt(pi)*erf(sqrt(bd)))
% Sbar = mean(real(sqrt((data-offset)^2-noisefloor^2))) 

% // P[1]: ODI
% // P[2]: dax
% // P[3]: th - fibre inclination
% // P[4]: phi - fibre azimuth
% // P[5]: F - signal fraction * S0
% // P[6]: offset
% // P[7]: quadratic noisefloor 
% i.e Y = sqrt((S+P[6]).^2+P[7].^2)

kappa = 1/tan(pi/2*P(1));       % Convert ODI to kappa
d = P(2);                       
th = P(3);
phi = P(4);
mu = [sin(th)*cos(phi);sin(th)*sin(phi);cos(th)];  
F = P(5);           % A param of the model, or calc'd via spherical mean
offset = P(6);
noisefloor = P(7);

if P(1)==0
    A = exp(-b.*d.*(g'*mu).^2);                 % Stick
else
    D = eig(-b.*d.*(g*g') + kappa.*(mu*mu'));   % Watson
    A = hyp_Sapprox(D)./hyp_Sapprox(eig(kappa.*(mu*mu')));
end

% // If selected, calc F via spherical mean
if isfield(const,'smt') && const.smt
    Sb = const.S(const.bvals==b);  
    if noisefloor==0
        sm = mean(Sb-offset);
    else
        sm = mean(real(sqrt(Sb.^2-noisefloor.^2))-offset);  
    end
    rtbd = sqrt(b*d);
    F = sm*2*rtbd/(sqrt(pi)*erf(rtbd));
end

S = F*A;

Y = sqrt((S+offset).^2+noisefloor.^2);      % DC offset & Rician noise


end