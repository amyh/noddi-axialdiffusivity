#!/bin/bash
# Bash script to run modified NODDI model on simulated data usng the fmrib cluster
# Simulate both real-valued and magnitude data
# Test various assumptions as shows in paper e.g. parameter accuracy and 
# precision as a function of SNR, and what happens when we get the noise 
# parameters wrong

module add MATLAB/2020a
queue=short.q
rep=20

# Fit to simulated data for different number of voxels (e.g. different SNR)

fixedoffset=[]
for noisemodel in Gaussian Rician; do
    for Nvox in 1 25 50 100 200 500 750 1000; do
        if [[ $noisemodel == 'Gaussian' ]]; then
            fixednoisefloor=0
            tmp=$fixednoisefloor
        else
            fixednoisefloor=[]
            tmp=''
        fi
        mkdir -p simulations/scripts/logs
        s=$PWDscripts/simulations/${noisemodel}_Nvox${Nvox}_rep${rep}_offset_noisefloor${tmp}.m
        echo "addpath(genpath('$PWD'))" > $s
        echo "run_simulations('${noisemodel}',${Nvox},${rep},${fixedoffset},${fixednoisefloor})" >> $s
        chmod 755 $s
        ~/func/matsub.sh $s -N sim_${noisemodel}_Nvox${Nvox}_rep${rep}_offset_noisefloor${tmp} -l simulations/scripts/logs -q $queue
    done
done

# Also test simplified model (offset=0)
fixedoffset=0
for noisemodel in Gaussian Rician; do
    for Nvox in 1 25 50 100 200 500 750 1000; do
        if [[ $noisemodel == 'Gaussian' ]]; then
            fixednoisefloor=0
            tmp=$fixednoisefloor
        else
            fixednoisefloor=[]
            tmp=''
        fi
        mkdir -p simulations/scripts/logs
        s=$PWDscripts/simulations/${noisemodel}_Nvox${Nvox}_rep${rep}_offset${fixedoffset}_noisefloor${tmp}_GToffset0.m
        echo "addpath(genpath('$PWD'))" > $s
        echo "run_simulations('${noisemodel}',${Nvox},${rep},${fixedoffset},${fixednoisefloor},0)" >> $s
        chmod 755 $s
        ~/func/matsub.sh $s -N sim_${noisemodel}_Nvox${Nvox}_rep${rep}_offset0_noisefloor${tmp} -l simulations/scripts/logs -q $queue
    done
done

# What happens when we get some noise parameters wrong?
Nvox=100

# Real-valued data, assume fixed offset
noisemodel=Gaussian
fixednoisefloor=0
for fixedoffset in {0..15}; do   
    s=$PWDscripts/simulations/${noisemodel}_Nvox${Nvox}_rep${rep}_offset${fixedoffset}_noisefloor${fixednoisefloor}.m
    echo "addpath(genpath('$PWD'))" > $s
    echo "run_simulations('${noisemodel}',${Nvox},${rep},${fixedoffset},${fixednoisefloor})" >> $s
    chmod 755 $s
    ~/func/matsub.sh $s -N sim_${noisemodel}_Nvox${Nvox}_rep${rep}_offset${fixedoffset}_noisefloor${fixednoisefloor} -l simulations/scripts/logs -q $queue
done
    
# Magnitude data, assume fixed offset
noisemodel=Rician
fixednoisefloor=[]
for fixedoffset in {0..15}; do   
    s=$PWDscripts/simulations/${noisemodel}_Nvox${Nvox}_rep${rep}_offset${fixedoffset}_noisefloor.m
    echo "addpath(genpath('$PWD'))" > $s
    echo "run_simulations('${noisemodel}',${Nvox},${rep},${fixedoffset},${fixednoisefloor})" >> $s
    chmod 755 $s
    ~/func/matsub.sh $s -N sim_${noisemodel}_Nvox${Nvox}_rep${rep}_offset${fixedoffset}_noisefloor -l simulations/scripts/logs -q $queue
done

# Magnitude data, assume noisefloor
# To simplify things, set offset=0
noisemodel=Rician
fixedoffset=0
offset=0
for fixednoisefloor in `seq 0 2 30`; do   
    s=$PWDscripts/simulations/${noisemodel}_Nvox${Nvox}_rep${rep}_offset${fixedoffset}_noisefloor${fixednoisefloor}.m
    echo "addpath(genpath('$PWD'))" > $s
    echo "run_simulations('${noisemodel}',${Nvox},${rep},${fixedoffset},${fixednoisefloor},${offset})" >> $s
    chmod 755 $s
    ~/func/matsub.sh $s -N sim_${noisemodel}_Nvox${Nvox}_rep${rep}_offset${fixedoffset}_noisefloor${fixednoisefloor} -l simulations/scripts/logs -q $queue
done

# Simplified model with offset of 0 but with noisefloor as a parameter of the model
noisemodel=Rician
fixedoffset=0
offset=0
fixednoisefloor=[]
s=$PWDscripts/simulations/${noisemodel}_Nvox${Nvox}_rep${rep}_offset${fixedoffset}_noisefloor.m
echo "addpath(genpath('$PWD'))" > $s
echo "run_simulations('${noisemodel}',${Nvox},${rep},${fixedoffset},${fixednoisefloor},${offset})" >> $s
chmod 755 $s
~/func/matsub.sh $s -N sim_${noisemodel}_Nvox${Nvox}_rep${rep}_offset${fixedoffset}_noisefloor -l simulations/scripts/logs -q $queue


# Simplified model with offset of 10 but with noisefloor as a parameter of the model
# i.e. what happens if there is an offset but in magnitude data we assume there isnt
noisemodel=Rician
fixedoffset=0
offset=10
fixednoisefloor=[]
for Nvox in 1 25 50 100 200 500 750 1000; do
    s=$PWDscripts/simulations/${noisemodel}_Nvox${Nvox}_rep${rep}_offset${fixedoffset}_noisefloor_GToffset${offset}.m
    echo "addpath(genpath('$PWD'))" > $s
    echo "run_simulations('${noisemodel}',${Nvox},${rep},${fixedoffset},${fixednoisefloor},${offset})" >> $s
    chmod 755 $s
    ~/func/matsub.sh $s -N sim_${noisemodel}_Nvox${Nvox}_rep${rep}_offset${fixedoffset}_noisefloor -l simulations/scripts/logs -q $queue
done







