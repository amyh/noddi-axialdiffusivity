function save_fig(fname,res)

if ~exist('res','var')
    res = 0;
end

switch res
    case 0
        print([fname '.jpeg'],'-djpeg')       
    case 1        
        print([fname '.jpeg'],'-djpeg','-r600')
end

print([fname '.eps'],'-depsc')

savefig([fname '.fig'])

set(gcf, 'Color', 'w')
export_fig(fname,'-pdf','-r600','-noinvert')

