# NODDI-AxialDiffusivity

Code relating to the paper "Estimating axial diffusivity in the NODDI model", Howard et al.

The study first examines how the output of NODDI is dependent on the 
assumed axial diffusivity, using HCP data. 
- The HCP data can be openly accessed via http://www.humanconnectomeproject.org/.

- All code can be found at ./Code/hcp

- The NODDI model is ran using the cudimot framework, available at
https://users.fmrib.ox.ac.uk/~moisesf/cudimot/.
Here we modify the cudimot NODDI model to accept a user-defined axial 
diffusivity. Code for which can be found in ./Code/hcp/cudimot/mymodels and
which is ran using the script ./Code/hcp/run-NODDI.sh.


Second, the study asks whether we can use a modified NODDI model for high
b-value data to estimate axial diffusivity on a per subject basis. Here
we apply the modified NODDI model to both magnitude and real-valued 
data from the MGH connectom scanner. We further investigate the accuracy 
and precision of our results using simulated data.

- The MGH data for this study 'MGH-RealValued' can be openly accessed via
https://www.nature.com/articles/s41597-021-01092-6

- Code to preprocess the data: ./Code/preprocessing and 
./Code/submit2cluster-preprocessing.sh

- Code to fit the model to invivo data: ./Code/run_modelling.m and 
./Code/submit2cluster-modelling.sh

- Code to fit to simulated data: ./Code/run_simulations.m and 
./Code/submit2cluster-simulations.sh

- submit2cluster-* writes and submits scripts to reproduce the results in 
the paper using the fmrib computing cluster. 

To run the code on alternative data, please modify run_modelling or 
run_simulations which have hardcoded paths that assume the data is in 
./Data/MGH-RealValued and ./Data/HCP

All figures from the paper can be reproduced using the scripts in 
./Code/plotting . The optimised model parameters (see 'samples' 
variable in .mat files), but not the original
data (which can be downloaded elsewhere) are available in ./Data
