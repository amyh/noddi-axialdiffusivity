%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% mcmc %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

function samples = mcmc(files,const,grid)

    % Run metropolis hastings optimisation to output parameter distribution
    % => can analyse degeneracy
    % Use output from gridsearch as starting values
    
    % Default, fix F to 0 as not param of moodel
    params_init = [grid.max.ODI;grid.max.dax;const.th;const.phi;...
        0;grid.max.offset;grid.max.noisefloor];
    ub = [1;4;const.th;const.phi;0;0.5*const.S0;0.5*const.S0];
    lb = [0;0;const.th;const.phi;0;0;0];
    
    % If priors flag=1, restrict ub and lb to of offset and noisefloor to be
    % 50 - 150% of const.noisemu and const.noiseg, which in real data, are
    % estimated from the ventricles
    if isfield(const,'priors') && const.priors==1
        tol = 0.5;
        ub(6) = const.noisemu*(1+tol);
        lb(6) = const.noisemu*(1-tol);
        ub(7) = const.noiseg*(1+tol);
        lb(7) = const.noiseg*(1-tol);
    end
          
    if const.smt~=1
        % Also estimate F
        % Calculate F approx from gridsearch
        b = unique(const.bvals);
        for i = 1:numel(b)
            Sb = const.S(const.bvals==b(i));
            % Correct for noisefloor
            if grid.max.noisefloor == 0
                sm = mean(Sb-grid.max.offset);
            else
                sm = mean(real(sqrt(Sb.^2-grid.max.noisefloor.^2))-grid.max.offset);
            end
            rtbd = sqrt(b(i)*grid.max.dax);
            F(i) = sm*2*rtbd/(sqrt(pi)*erf(rtbd));
        end
        grid.max.F = mean(F);

        params_init(5) = grid.max.F;
        ub(5) = const.S0;
        lb(5) = 0;
        sprintf('const.smt=0 => estimating F as a model parameter')
    end
    
    % Fix some paramaters if required
    if isfield(const,'fixedf') && ~isempty(const.fixedf)
        params_init(5) = const.fixedf*const.S0;
        ub(5) = const.fixedf*const.S0;
        lb(5) = const.fixedf*const.S0;
    end
    
    if ~isempty(const.fixedoffset)
        params_init(6) = const.fixedoffset;
        ub(6) = const.fixedoffset;
        lb(6) = const.fixedoffset;
    end
    
    if ~isempty(const.fixednoisefloor)
        params_init(7) = const.fixednoisefloor;
        ub(7) = const.fixednoisefloor;
        lb(7) = const.fixednoisefloor;
    end

    const.output = 'predict';
    opt = [];
    fun = @(params)(pred(params,const));
    samples = mh(const.S,params_init,fun,lb,ub,opt);

    save([files.out(1:end-4) '_mcmc.mat'])
    disp([files.out(1:end-4) '_mcmc.mat'])

end

function samples = mh(varargin)
% res = mh(y,x0,@genfunc,[LB,UB,params])
%
% Compulsory Parameters
%   y        = data (Nx1)
%   x0       = initial parameters (Px1)
%   @genfunc = generative model
%
%   Output is nsamples*P where nsamples=njumps/sampleevery
%
% Example:
%   % define a forward model (here y=a*exp(-bx))
%   myfun=@(x,c)(exp(-x(1)*c)+x(2));
%   % generate some noisy data
%   true_x = [1;2];
%   c=linspace(1,10,100);
%   y=myfun(true_x,c) + .05*randn(1,100);
%   % estimate parameters
%   x0=[1;2]; % you can get x0 using nonlinear opt
%   samples=mh(y,x0,@(x)(myfun(x,c)));
%   figure,plot(samples)
%
% Other Parameters
%   LB = lower bounds on x (default=[-inf]*ones(size(x0)))
%   UB = upper bounds on x (default=[+inf]*ones(size(x0)))
%   params.burnin = #burnin iterations (default=1000)
%   params.njumps = #jumps (default=5000)
%   params.sampleevery = keep every n jumps (default=10)
%   params.update = update proposal every n jumps (default=20)
%
%
% S. Jbabdi 01/12


[y,x0,genfunc,LB,UB,params]=parseinputargs(varargin,nargin);
N=length(y);

p=x0;
s=genfunc(x0);
e=N/2*log(norm(y-s));

acc=zeros(1,length(p));
rej=zeros(1,length(p));
keepp=zeros(params.njumps,length(p));
prop=ones(1,length(p));
for i=1:params.njumps+params.burnin
    for k=1:length(p)
        oldp=p;
        p(k)=p(k)+randn*prop(k);
        if(p(k)<LB(k) || p(k)>UB(k))
            p(k)=oldp(k);
            rej(k)=rej(k)+1;
        else
            s=genfunc(p);
            olde=e;
            e=N/2*log(norm(y-s));
            if(exp(olde-e)>rand)
                acc(k)=acc(k)+1;
            else
                p(k)=oldp(k);
                rej(k)=rej(k)+1;
                e=olde;
            end
        end
    end
    keepp(i,:)=p;
    if(rem(i,params.update)==0)
        prop=prop.*sqrt((1+acc)./(1+rej));
        acc=0*acc;
        rej=0*rej;
    end
end
samples=keepp(params.burnin+1:params.sampleevery:end,:);

end % EOF

function [y,x0,genfunc,LB,UB,params]=parseinputargs(varargin,nargin)
y=varargin{1};
x0=varargin{2};
genfunc=varargin{3};
if(nargin>3);LB=varargin{4};else LB=-inf*ones(size(x0));end
if(nargin>4);UB=varargin{5};else UB=+inf*ones(size(x0));end
if(nargin>5);
    params=varargin{6};
else
    params=struct('burnin',1000,'njumps',5000,'sampleevery',10,'update',20);
end
if(~isfield(params,'burnin'));params.burnin=1000;end
if(~isfield(params,'njumps'));params.njumps=5000;end
if(~isfield(params,'sampleevery'));params.sampleevery=10;end
if(~isfield(params,'update'));params.update=20;end

end % EOF