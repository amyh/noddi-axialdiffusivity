# Outputs from the standard NODDI model

Here we have the mean NODDI maps across subjects in MNI space. 
For each HCP subject, NODDI maps were produced for various assumed axial diffusivity:
dax = 1.7, 2.3 and 3 microns^2/ms
The maps were then warped to standard space, where here we have the mean map 
across subjects "<output>_mean.nii.gz" for each axial diffusivity.
