#!/bin/bash
# Script to send preprocessing to fmrib cluster

for i in MGH_001 MGH_002 MGH_003 MGH_004 MGH_005 MGH_009; do 
  fsl_sub -q bigmem.q -N preprocessing_$i -l logs/ ./preprocessing/mgh_preprocessing.sh $i
done


