%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% simulate_data %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

function const = simulate_data(const,sim)

% Simulate data for a given 'const' which has previously been loaded from
% data => match S0 noise etc.
% Data is simulated for N voxels and can be either concatenated or averaged

% If averaging, simulate data for fixed fibre orientation
if sim.average
    fixeddir = 1;
    th = rand*pi;
    phi = rand*2*pi;
else
    fixeddir = 0;
end

if ~isempty(sim.SNR) 
    const.noiseg = const.S0/sim.SNR;   
end

smt = const.smt;
    
S = [];
bvecs = [];
bvals = [];

% bvecs and bvals for a single voxel
const.bvals = const.bvals(:,1:numel(const.S)/numel(const.N));
const.bvecs = const.bvecs(:,1:numel(const.S)/numel(const.N));

% simulate data for each voxel and concatenate output
for i = 1:sim.Nvox
    
    if ~fixeddir
        th = rand*pi;
        phi = rand*2*pi;
    end
    
    mu = [sin(th)*cos(phi);sin(th)*sin(phi);cos(th)];
    const.output = 'predict';
    const.smt = 0;

    % simulate S for given GT parameters
    p = pred([sim.GT.ODI;sim.GT.dax;th;phi;sim.GT.f*const.S0;sim.GT.offset;0],const);
    
    % Rotate each voxel to align with z axis
    rvec = vrrotvec(mu,[0;0;1]);
    rmat1 = vrrotvec2mat(rvec); 
    
    S = [S;p];
    bvecs = [bvecs,rmat1*const.bvecs];
    bvals = [bvals,const.bvals]; 
    
end

% add noise
if strcmp(sim.noisemodel,'Gaussian')
    const.S = S+randn(size(S))*const.noiseg;
    const.noisemu = sim.GT.offset;      
    const.noisef = sim.GT.offset;      
elseif strcmp(sim.noisemodel,'Rician')
    const.S = abs(S+randn(size(S))*const.noiseg+...
        (-1)^0.5*randn(size(S))*const.noiseg);
    const.noisemu = sim.GT.offset;       % Rician scale parameter
    [const.noisef,~] = ricestat(sim.GT.offset,const.noiseg); % Mean signal of rician distribution
end

% replace data in const with simulated data
const.bvals = bvals;
const.bvecs = bvecs;
const.S0 = mean(const.S0all);
const.th = 0;
const.phi = 0;
const.N = 1:sim.Nvox;
const.smt = smt;

% average data across voxels
if sim.average
    const = calc_av_signal(const);
end

% stop infinities & nans
if const.noiseg==0
    const.noiseg = eps;
end

end