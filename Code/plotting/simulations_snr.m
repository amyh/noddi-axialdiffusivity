%% HOW DO RESULTS DEPEND ON SNR?

% Here we have fitted to simulated data where the data has been averaged
% across N voxels, where each voxel has an SNR=16.5
% We perform 20 repetitions for each N=[1,25,50,100,1000]

% Plot the estimated parameters against N to see how precision and accuracy
% change as a function of SNR

clear all
addpath(genpath('../'))
[cb] = cbrewer('qual','Set3',10,'pchip');
col = [cb(5,:);cb(7,:);cb(6,:);cb(4,:);cb(8,:)];

% Folder with simulated data
f = '../../Data/MGH-RealValued/MGH_002/diff_real/Delta49/HighB/simulations/';

noise = 'Gaussian';
noise = 'Rician';
model = 'w-offset';
model = 'wo-offset';   % Only tested for Rician

if strcmp(noise,'Gaussian')
    base = 'ModifiedNODDI_priors0_smt1_1-100_offset_noisefloor0';    
elseif strcmp(noise,'Rician') && strcmp(model,'w-offset')
    base = 'ModifiedNODDI_priors1_smt1_1-100_offset_noisefloor';     
elseif strcmp(noise,'Rician') && strcmp(model,'wo-offset') 
    base = 'ModifiedNODDI_priors0_smt1_1-100_offset0_noisefloor';     
end

GTdax = 2.2;

simNall = [1 25 50 100 200 500 750 1000];

for jj = 1:numel(simNall)
    simN = simNall(jj); 
    for rep = 1:10
        
        if strcmp(model,'w-offset')
            foutsim = [f '/' base '_simNvox' num2str(simN) '_simSNR16.5_' noise '_rep' num2str(rep) '_GT0.03_2.2_0.6_10_mcmc.mat'];
        elseif strcmp(model,'wo-offset')
            foutsim = [f '/' base '_simNvox' num2str(simN) '_simSNR16.5_' noise '_rep' num2str(rep) '_GT0.03_2.2_0.6_0_mcmc.mat'];
        end
        load(foutsim)
        
        s = mean(samples,1);
        sstd = std(samples,[],1);
        ODI(jj,rep) = s(1); 
        ODIstd(jj,rep) = sstd(1);
        dax(jj,rep) = s(2);
        daxstd(jj,rep) = sstd(2);
        offset(jj,rep) = s(6);
        offsetstd(jj,rep) = sstd(6);
        noisefloor(jj,rep) = s(7);
        noisefloorstd(jj,rep) = sstd(7);
        
    end
    
end
   
disp('Data loaded')

%%%

%%figure('units','normalized','outerposition',[0 0 1 1])
figure
SNRall = floor(16.5*sqrt(simNall));
x = 1:600;

subplot(2,4,1),hold on
l = plot(x,ones(size(x))*GTdax);
set(l,'Color',[0.5,0.5,0.5])
set(l,'LineStyle','--')
% set(l,'LineWidth',1)
b = plot(SNRall,dax,'.','MarkerSize',8,'Color',col(4,:));
plot(SNRall,mean(dax,2),'.','MarkerSize',18,'Color',[0.5,0.5,0.5])
% ylim([1.4,3.2])
ylim([1.5,3.5])
axis square
xlabel('SNR')
ylabel('Axial diffusivity')
box on

subplot(2,4,2),hold on
l = line(x,ones(size(x))*0.03);
set(l,'Color',[0.5,0.5,0.5])
set(l,'LineStyle','--')
% set(l,'LineWidth',1)
b = plot(SNRall,ODI,'.','MarkerSize',8,'Color',col(4,:));
plot(SNRall,mean(ODI,2),'.','MarkerSize',18,'Color',[0.5,0.5,0.5])
ylim([0.01,0.05])
axis square
xlabel('SNR')
ylabel('ODI')
box on

subplot(2,4,3),hold on
l = line(x,ones(size(x))*10);
set(l,'Color',[0.5,0.5,0.5])
set(l,'LineStyle','--')
b = plot(SNRall,offset,'.','MarkerSize',8,'Color',col(4,:));
plot(SNRall,mean(offset,2),'.','MarkerSize',18,'Color',[0.5,0.5,0.5])
ylim([0,20])
axis square
xlabel('SNR')
ylabel('Offset')
box on

subplot(2,4,4),hold on
l = line(x,ones(size(x))*const.S0/16.5);
set(l,'Color',[0.5,0.5,0.5])
set(l,'LineStyle','--')
b = plot(SNRall,noisefloor,'.','MarkerSize',8,'Color',col(4,:));
plot(SNRall,mean(noisefloor,2),'.','MarkerSize',18,'Color',[0.5,0.5,0.5])
ylim([0,20])
axis square
xlabel('SNR')
ylabel('Noisefloor')
box on

subplot(2,4,5),hold on
b = plot(SNRall,daxstd,'.','MarkerSize',8,'Color',col(4,:));
plot(SNRall,mean(daxstd,2),'.','MarkerSize',18,'Color',[0.5,0.5,0.5])
ylim([0,1])
axis square
xlabel('SNR')
ylabel('Axial diffusivity std')
box on

subplot(2,4,6),hold on
b = plot(SNRall,ODIstd,'.','MarkerSize',8,'Color',col(4,:));
plot(SNRall,mean(ODIstd,2),'.','MarkerSize',18,'Color',[0.5,0.5,0.5])
ylim([0,0.015])
axis square
xlabel('SNR')
ylabel('ODI std')
box on

%%
%%%%%% Also show degeneracy as a distribution

simNall = [1 200];
for jj = 1:numel(simNall)
    simN = simNall(jj); 
    for rep = 1
        
        if strcmp(model,'w-offset') || strcmp(model,'ignored-offset')
            foutsim = [f '/' base '_simNvox' num2str(simN) '_simSNR16.5_' noise '_rep' num2str(rep) '_GT0.03_2.2_0.6_10_mcmc.mat'];
        elseif strcmp(model,'wo-offset')
            foutsim = [f '/' base '_simNvox' num2str(simN) '_simSNR16.5_' noise '_rep' num2str(rep) '_GT0.03_2.2_0.6_0_mcmc.mat'];
        end
        load(foutsim)
        
        subplot(2,4,6+jj)
        %plot(samples(:,1),samples(:,2),'x','Color',col(4,:))
        dscatter(samples(:,1),samples(:,2))
        ylim([0,4])
        xlim([0,0.1])
        
        xlabel('ODI')
        ylabel('Axial diffusivty')
        axis square
        box on
        
    end
    
end

set(findall(gcf,'-property','FontSize'),'FontSize',14)


%%
disp(['Figures/Simulations-snr-' noise '-' model])
save_fig(['Figures/Simulations-snr-' noise '-' model],1)
