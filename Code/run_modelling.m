
function run_modelling(sub,diffreal,Nvox,offset)

% Run modified NODDI model on invivo data
% sub       subject
% diffreal  diff (if magnitude data) / diff_real (if real-valued data)
% Nvox      number of voxels to concatenate and fit to
% offset    assumed signal offset. If left blank, the offset is estimated 
%           as a parameter of the model.

% Set paths for fsl read_avw / write_avw
cluster = 1;
if cluster==1
    setenv( 'FSLDIR', '/opt/fmrib/fsl');
    fsldir = getenv('FSLDIR');
    fsldirmpath = sprintf('%s/etc/matlab',fsldir);
    path(path, fsldirmpath);
    clear fsldir fsldirmpath;
else  
    setenv( 'FSLDIR', '/usr/local/fsl');
    fsldir = getenv('FSLDIR');
    fsldirmpath = sprintf('%s/etc/matlab',fsldir);
    path(path, fsldirmpath);
    clear fsldir fsldirmpath;
end

% Files
files.main = ['../Data/MGH-RealValued/' sub '/' diffreal '/Delta49/HighB/'];
files.mask = ['../Data/MGH-RealValued/' sub '/' diffreal '/masks/cc/AllCC_dil.nii.gz'];
files.dti = ['../Data/MGH-RealValued/' sub '/' diffreal '/Delta49/LowB.dtifit/'];
files.vent = ['../Data/MGH-RealValued/' sub '/' diffreal '/Delta49/b17800/data_ventricles.nii.gz'];

% Constant variables
const.N = 1:Nvox;
if strcmp(diffreal,'diff_real')     % fit to real-valued data
    const.rician = 0;               % assume Gaussian
    const.fixedoffset = [];         % estimate as parameter of model
    const.fixednoisefloor = 0;      % Gaussian => no rician noisefloor
elseif strcmp(diffreal,'diff')      % fit to magnitude data
    const.rician = 1;               % assume Rician
    if ~exist('offset','var'); offset=[]; end
    const.fixedoffset = offset;     % can also optimise assuming some fixed offset
    const.fixednoisefloor = [];     % esimate as parameter of model
end
const.smt = 1;                      % replace F=fin*S0 with spherical mean

% Priors can be used to constrain the parameter space (ub/lb) of the offset
% and noisefloor. Flag should be set to 0 if only one noise parameter is 
% estimated, and 1 if both are estimated
if isempty(const.fixednoisefloor) && isempty(const.fixedoffset)
    const.priors = 1;               % Restrict ub/.lb of offset and noisefloor according to values estimated from the ventricles
else
    const.priors = 0;
end
const.excess = 0.1;                 % Select voxels with S0 +- 10%

% Do not simulate data
sim.flag = 0;

files.out = [files.main '/ModifiedNODDI_symm_priors' num2str(const.priors) '_smt' num2str(const.smt) '_' num2str(min(const.N)) '-' num2str(max(const.N)) '_offset' num2str(const.fixedoffset) '_noisefloor' num2str(const.fixednoisefloor) '.mat'];

% Parameters for gridsearch (prior to mcmc)
grid.dax_s = linspace(1.5,3,16);
grid.ODI_s = linspace(0.01,0.07,16);
grid.noisefloor_s = linspace(0,20,11);
grid.offset_s = linspace(0,20,11);

% Do not plot output
plotflag = 0;

% Run model
run_modified_noddi(files,const,sim,grid,plotflag)

end