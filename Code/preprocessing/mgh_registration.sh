#!/bin/bash

sub=$PWD/../../Data/MGH-RealValued/$1
p=$sub/$2/Delta49/
s=$sub/t1w/
#outdir=$p/LowB.bedpostX/xfms
outdir=$sub/$2/reg/

mkdir -p $outdir

refbrain=$FSLDIR/data/standard/MNI152_T1_2mm_brain.nii.gz
ref=$FSLDIR/data/standard/MNI152_T1_2mm.nii.gz

# Linear registration struct to standard
$FSLDIR/bin/flirt -in $s/${1}_t1w_biascorr_brain.nii.gz -ref $refbrain -omat $outdir/str2standard.mat -searchrx -90 90 -searchry -90 90 -searchrz -90 90 -dof 12 -cost corratio

# inverse transform
$FSLDIR/bin/convert_xfm -omat $outdir/standard2str.mat -inverse $outdir/str2standard.mat

# concat transforms
$FSLDIR/bin/convert_xfm -omat $outdir/diff2standard.mat -concat $outdir/str2standard.mat $sub/diff_real/${1}_diff_real2t1w.txt

# inverse transform
$FSLDIR/bin/convert_xfm -omat $outdir/standard2diff.mat -inverse $outdir/diff2standard.mat

# Nonlinear registration struct to standard
$FSLDIR/bin/fnirt --in=$s/${1}_t1w_biascorr.nii.gz --aff=$outdir/str2standard.mat --cout=$outdir/str2standard_warp --config=T1_2_MNI152_2mm

# Invert and concat warps
$FSLDIR/bin/invwarp -w $outdir/str2standard_warp -o $outdir/standard2str_warp -r $s/${1}_t1w_biascorr_brain.nii.gz

$FSLDIR/bin/convertwarp -o $outdir/diff2standard_warp -r $ref -m $sub/diff_real/${1}_diff_real2t1w.txt -w $outdir/str2standard_warp

$FSLDIR/bin/convertwarp -o $outdir/standard2diff_warp -r $p/nodif_brain_mask -w $outdir/standard2str_warp --postmat=$sub/diff_real/${1}_t1w2diff_real.txt

# Apply warps so can asses quality of registration
$FSLDIR/bin/applywarp -i $p/nodif_brain_mask -r $ref -o $outdir/diff2standard -w $outdir/diff2standard_warp
$FSLDIR/bin/applywarp -i $s/${1}_t1w_biascorr.nii.gz -r $ref -o $outdir/str2standard -w $outdir/str2standard_warp
