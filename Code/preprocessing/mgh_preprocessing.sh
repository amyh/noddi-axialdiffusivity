code=$PWD
s=$1	# MGH_001 etc
sub=$PWD/../../Data/MGH-RealValued/$s

for fold in diff diff_real; do

bvals=$sub/${fold}/${s}_${fold}_bvals
bvecs=$sub/${fold}/${s}_${fold}_bvecs
delta=$sub/${fold}/${s}_${fold}_Deltas
data=$sub/${fold}/${s}_${fold}.nii.gz
mask=$sub/${fold}/${s}_${fold}_mask.nii.gz

# Extract delta49 and delta0 data
unlink ${delta}.T
unlink ${bvals}.T
unlink ${bvecs}.T
~/func/transpose_bvecs.sh ${delta}.txt >  ${delta}.T
~/func/transpose_bvecs.sh ${bvals}.txt > ${bvals}.T
~/func/transpose_bvecs.sh ${bvecs}.txt > ${bvecs}.T
mkdir -p $sub/${fold}/Delta49; 
mkdir -p $sub/${fold}/Delta0; 
~/func/extract_shell.py -d $data -b  ${delta}.T -v ${bvecs}.T -s 49 -a 5 -o $sub/${fold}/Delta49/
~/func/extract_shell.py -d $data -b  ${delta}.T -v ${bvecs}.T -s 0 -a 5 -o $sub/${fold}/Delta0/
# Correct bvals
rm $sub/${fold}/Delta49/bvals.T
while read -r d && read -r b <&3; do 
  if [[ $d == "49" ]]; then echo $b >> $sub/${fold}/Delta49/bvals.T; fi; 
done <${delta}.txt 3<${bvals}.txt
 ~/func/transpose_bvecs.sh $sub/${fold}/Delta49/bvals.T > $sub/${fold}/Delta49/bvals
ln -s $mask $sub/${fold}/Delta49/nodif_brain_mask.nii.gz


# COPY DATA & EXTRACT SHELLS
cd $sub/${fold}/Delta49; 
for i in 950 6750 9850 13500 17800; do mkdir b$i; ~/func/extract_shell.py -d data.nii.gz -b bvals -v bvecs -s $i -o b$i/ -a 50; done
ln -s $sub/${fold}/Delta0 b0
fslmaths b0/data.nii.gz -Tmean b0/S0
mkdir HighB
fslmerge -t HighB/data b6750/data.nii.gz b9850/data.nii.gz b13500/data.nii.gz 
~/func/transpose_bvecs.sh b6750/bvals > tmp
~/func/transpose_bvecs.sh b9850/bvals >> tmp
~/func/transpose_bvecs.sh b13500/bvals >> tmp
~/func/transpose_bvecs.sh tmp > HighB/bvals
~/func/transpose_bvecs.sh b6750/bvecs > tmp
~/func/transpose_bvecs.sh b9850/bvecs >> tmp
~/func/transpose_bvecs.sh b13500/bvecs >> tmp
~/func/transpose_bvecs.sh tmp > HighB/bvecs
ln -s $PWD/nodif_brain_mask.nii.gz HighB/
mkdir LowB
fslmerge -t LowB/data b0/data.nii.gz b950/data.nii.gz 
~/func/transpose_bvecs.sh b0/bvals > tmp
~/func/transpose_bvecs.sh b950/bvals >> tmp
~/func/transpose_bvecs.sh tmp > LowB/bvals
~/func/transpose_bvecs.sh b0/bvecs > tmp
~/func/transpose_bvecs.sh b950/bvecs >> tmp
~/func/transpose_bvecs.sh tmp > LowB/bvecs
ln -s $PWD/nodif_brain_mask.nii.gz LowB/
rm tmp

# DTIFIT & BEDPOSTX
mkdir LowB.dtifit
dtifit -k LowB/data.nii.gz -b LowB/bvals -r LowB/bvecs -m nodif_brain_mask.nii.gz -o LowB.dtifit/dti
#module add fsl_sub
#bedpostx LowB

if [[ $diffreal == "diff" ]]; then

  # REGISTRATION
  cd $code
  ./mgh_registration.sh $s $fold

  # WHITE MATTER MASK
  cd  $sub/${fold}/
  mkdir masks
  mkdir $sub/t1w/fast;
  fslmaths $sub/t1w/${s}_t1w_biascorr -mul $sub/t1w/${s}_t1w_brainmask $sub/t1w/${s}_t1w_biascorr_brain
  fast -t 1 -g -N -o $sub/t1w/fast/${s}_t1w_biascorr_brain $sub/t1w/${s}_t1w_biascorr_brain.nii.gz
  mask=$sub/${fold}/${s}_${fold}_mask.nii.gz
  flirt -in $sub/t1w/fast/${s}_t1w_biascorr_brain_seg_2 -ref $mask -applyxfm -init ${s}_t1w2${fold}.txt -out masks/wm_mask
  fslmaths masks/wm_mask.nii.gz -thr 0.5 -bin masks/wm_mask.nii.gz

  # CC MASKS
  cc=$sub/${fold}/masks/cc
  mkdir $cc
  cd $cc
  reg=$sub/${fold}/reg
  applywarp -i /home/fs0/amyh/func/CC_segmentation_HoferFrahm/MNI2mm/CombinedSections -o AllCC -w $reg/standard2diff_warp.nii.gz -r $sub/${fold}/Delta49/nodif_brain_mask
  for i in  *; do fslmaths $i -thr 0.5 -bin $i; done
  fslmaths AllCC -dilM AllCC_dil

  # VENTRICLE MASK
  in=$code/../../Data/MNI152_T1_2mm_ventricles_mask_ero.nii.gz 
  applywarp -i $in -o $sub/${fold}/masks/ventricles_mask -r $sub/${fold}/Delta49/nodif_brain_mask -w $reg/standard2diff_warp.nii.gz
  fslmaths $sub/${fold}/masks/ventricles_mask -thr 0.8 -bin $sub/${fold}/masks/ventricles_mask


# ERODE MASKS
#cd $sub//${fold}/masks/cc
#for i in *; do fslmaths $i -ero ${i%.nii.gz}-ero; done

else
  ln -s $sub/diff/masks $sub//${fold}/;
  ln -s $sub/diff/reg $sub//${fold}/;
fi

fslmaths $sub/${fold}/Delta49/b0/S0 -mul $sub/${fold}/masks/wm_mask $sub/${fold}/Delta49/HighB/S0_wm
fslmaths $sub/${fold}/Delta49/b17800/data -mul $sub/${fold}/masks/ventricles_mask $sub/${fold}/Delta49/b17800/data_ventricles

done


