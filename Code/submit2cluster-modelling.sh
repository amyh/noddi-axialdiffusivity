#!/bin/bash
# Script to run modified NODDI model on in vivo data usng the fmrib cluster
# Model is fitted to N=1/25/50/100/200 concatenated voxels from both 
# real-valued and magnitude data

module add MATLAB/2020a
folder=$PWD/../Data/MGH-RealValued/
p=$PWD/
mkdir -p ${p}/scripts/modelling/logs

for diffreal in diff diff_real; do
  mask=AllCC_dil	
  
  for i in {1..5} 9; do
    sub=MGH_00${i}
    for n in 1 25 50 100 200; do
      queue=verylong.q
      if [[ ${n} == 1 ]]; then queue=short.q; fi
      #if [[ ${n} == 25 ]]; then queue=long.q; fi
      out=${p}/scripts/modelling/run_${sub}_${mask}_${n}_${diffreal}.m
      echo "addpath(genpath('$p'))" > $out
      echo "run_modelling('${sub}','${diffreal}',${n})"	>> $out
      chmod +x $out
      fsl_sub -q ${queue} -l $p/scripts/modelling/logs/ -N ${sub}_${mask}_${n}_${diffreal} matlab -singleCompThread -nodisplay -nosplash \< $out
    done
  done
done

#####################
# Offset = 0 in magnitude data
module add MATLAB/2020a
folder=$PWD/../Data/MGH-RealValued/
p=$PWD/
mkdir -p ${p}/scripts/modelling/logs
for diffreal in diff; do
  mask=AllCC_dil	
  
  for i in {1..5} 9; do
    sub=MGH_00${i}
    for n in 1 25 50 100 200; do
      queue=verylong.q
      if [[ ${n} == 1 ]]; then queue=short.q; fi
      if [[ ${n} == 25 ]]; then queue=long.q; fi
      out=${p}/scripts/modelling/run_${sub}_${mask}_${n}_${diffreal}_offset0.m
      echo "addpath(genpath('$p'))" > $out
      echo "run_modelling('${sub}','${diffreal}',${n},0)"	>> $out
      chmod +x $out
      fsl_sub -q ${queue} -l $p/scripts/modelling/logs/ -N ${sub}_${mask}_${n}_${diffreal}_offset0 matlab -singleCompThread -nodisplay -nosplash \< $out
    done
  done
done

