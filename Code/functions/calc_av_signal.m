%%%%%%%%%%%%%%%%%%%%%%%%% calc_av_signal %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


function const = calc_av_signal(const)

% Calculate average signal for each bvalue

B = unique(const.bvals);
nbvecs = numel(const.S)/numel(const.N)/numel(B);
S = [];
bvecs = [];
bvals = [];

for i = 1:numel(B)
    
    Sb = const.S(const.bvals==B(i));
    Sb = reshape(Sb,nbvecs,[]);
    S = [S;mean(Sb,2)];
    v = const.bvecs(:,const.bvals==B(i));
    b = const.bvals(const.bvals==B(i));
    bvecs = [bvecs,v(:,1:nbvecs)];
    bvals = [bvals,b(1:nbvecs)];
        
end

const.S = S;
const.bvecs = bvecs;
const.bvals = bvals;

end
