%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% pred %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

function out = pred(params,const)

    % Calculate signal for each gradient direction
    % const.output      = predict => outputs signal, = cost => outputs SSE

    if ~isfield(const,'noisefloor')
        const.noisefloor = 0;
    end

    if ~isfield(const,'output')
        const.output = 'predict';
    end
    
    if params(5)~=0     % If F>0, estimate as param of model
        const.smt = 0;
    end

    % Loop over gradient directions
    ndir = numel(const.bvals);
    Spred = nan(ndir,1);
    for i = 1:ndir
        Spred(i) = modified_NODDI(params,const.bvecs(:,i),const.bvals(i),const);
    end
    
    % Output predicted signal or L2 norm
    if strcmp(const.output,'predict')
        out = Spred;
    elseif strcmp(const.output,'cost')
        e = const.S-Spred;
        out = sum(e.^2);  
    end

end