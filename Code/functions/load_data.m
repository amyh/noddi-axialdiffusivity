%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% load_data %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

function const = load_data(files,const)

    % Load in vivo data
    % Select N voxels with high FA and similar S0 
    % Output mask showing where voxels are located
    % Rotate data for each voxel so V1 is aligned and V2 is random
    % Concatenate data across voxels
    % Calulcate noise parameters from data in ventricles
    
    
    data = double(read_avw([files.main '/data.nii.gz']));
    S0 = double(read_avw([files.main '/S0_wm.nii.gz']));     % MGH
    bvecs = load([files.main '/bvecs']);      % Gradient orientations
    bvals = load([files.main '/bvals']);
    
    % Load roi mask
    % Ensure its within wm mask
    wm = S0; wm(wm>0)  = 1;
    if exist(files.mask,'file')
        cc = double(read_avw(files.mask));
        mask = wm.*cc;
    else
        sprintf('No mask detected. Using wm mask from S0_wm')
        mask = wm;
    end
    mask(mask>0) = 1;
    mask(mask~=1) = 0;
    clear cc

    fa = double(read_avw([files.dti '/dti_FA.nii.gz'])).*mask;
    v1 = double(read_avw([files.dti '/dti_V1.nii.gz'])).*mask;
    S0 = S0.*mask;

    % Remove erroneously high voxels
    fa(fa>0.95) = 0;
    
    % Extract voxels with highest fa
    [~,faind] = sort(fa(:),'descend');
    
    % Adjust const.N if there aren't enough voxels in mask
    if numel(const.N)>numel(mask(mask==1))
       const.N = 1:numel(mask(mask==1));
    end
    
    % Restrict to similar S0
    tmp = S0(faind);
    if const.excess<1       % can either be defined as fraction of S0 or fixed number
        ub = mean(S0(faind(const.N)))*(1+const.excess);
        lb = mean(S0(faind(const.N)))*(1-const.excess);
    else
        ub = mean(S0(faind(const.N)))+const.excess;
        lb = mean(S0(faind(const.N)))-const.excess;
    end
    % Adjust const.N if there aren't enough voxels in mask
    if (sum(mask(:))-sum(tmp>0 & (tmp<lb | tmp>ub)))<numel(const.N)
        const.N = 1:(sum(mask(:))-sum(tmp>0 & (tmp<lb | tmp>ub)));
    end
    disp(['const.N = ' num2str(numel(const.N))])
    if const.N(1)>1
        const.N = const.N - sum(tmp(1:const.N(1)) < lb | tmp(1:const.N(1)) > ub);
    end
    faind(tmp<lb | tmp>ub) = [];
    clear tmp

    % Take const.N highest voxels
    d = reshape(data,[],size(data,4)); % DO NOT normalise
    d = d(faind(const.N),:);
    v1 = reshape(v1,[],size(v1,4));
    v1 = v1(faind(const.N),:);
    
    % Save voxel mask
    tmp = double(read_avw([files.dti '/dti_FA.nii.gz']));
    tmp(faind(const.N)) = 2;
    save_avw(tmp,[files.out(1:end-4) '.nii.gz'],'f',[1,1,1])
    system(['fslcpgeom ' files.dti '/dti_FA.nii.gz' ' ' files.out(1:end-4) '.nii.gz']);
       
    % For each voxel in mask, rotate bvecs to align v1 with z-axis
    % i.e. (th=0,phi=0)
    totbvecs = [];
    totbvals = [];
    totdata = [];

    % Account for dispersion asymmmetry
    v2 = double(read_avw([files.dti '/dti_V2.nii.gz'])).*mask;
    v2 = reshape(v2,[],size(v2,4));
    v2 = v2(faind(const.N),:);
    
    for ii = 1:numel(const.N)
        rvec = vrrotvec(v1(ii,:),[0;0;1]);
        rmat1 = vrrotvec2mat(rvec); 

        % Account for dispersion asymmmetry
        % Rotate v2 to ensure V2 are randomly oriented
        rotv2 = rmat1*v2(ii,:)';
        randth = rand*2*pi;      
        rvec = vrrotvec(rotv2,[sin(randth);cos(randth);0]);
        rmat2 = vrrotvec2mat(rvec); 
        totbvecs = [totbvecs,rmat2*(rmat1*bvecs)];       
        totbvals = [totbvals,bvals];
        totdata = [totdata,d(ii,:)];
        
    end
    
    % Noisefloor - average sig across voxels in CSF
    % This is only used to understand the data or to infom simulations,
    % not in the axial diffusivity model
    noise = double(read_avw(files.vent));
    if const.rician
        pd = fitdist(noise(noise>0),'Rician');
        noisemu = pd.s;
    else
        pd = fitdist(noise(noise~=0),'Normal'); 
        noisemu = pd.mu;
    end
    noiseg = pd.sigma;      
    noisef = mean(noise(noise~=0));
    
    % Output
    const.bvecs = totbvecs;
    const.bvals = totbvals./1000;
    const.S0all = S0(faind(const.N));
    const.S0 = mean(const.S0all);
    const.noisef = noisef;       
    const.noiseg = noiseg;  
    const.noisemu = noisemu;
    const.S = totdata';
    const.FAall = fa(faind(const.N));
    const.th = 0;
    const.phi = 0;
    disp(['SNR = ' num2str(const.S0/const.noiseg)])
    
   
end
